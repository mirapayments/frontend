// Import the necessary libraries and modules.
import React from 'react';
import { contactUs } from '../../utils/apiHandlers/contactUs';
import { Link, NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';
import Select from 'react-select';
import '../Home/Pages.css';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import CurrencyInput from 'react-currency-input-field';
import Spinner from 'react-spinner-material';

// Contact Page Component
const ContactPage = () => {
  const countriesOptions = [
    { value: 'Nigeria', label: 'Nigeria' },
    { value: 'Ghana', label: 'Ghana' },
    { value: 'Togo', label: 'Togo' },
    { value: 'South Africa', label: 'South Africa' },
    { value: 'Cameroun', label: 'Cameroun' },
    { value: 'Egypt', label: 'Egypt' },
    { value: 'Sudan', label: 'Sudan' },
    { value: 'Kenya', label: 'Kenya' },
  ];

  async function sendContactUsFormData(data, setSubmitting) {
    try {
      const resp = await contactUs(data);
      console.log(resp);
      if (resp.data.status) {
        Swal.fire({
          title: '',
          text: resp.data.detail,
          icon: 'success',
          showConfirmButton: false,
          timer: 3000,
        });
        setSubmitting(false);
      } else if (!resp.data.status) {
        Swal.fire({
          title: '',
          text: 'There was an error sending the mail',
          icon: 'error',
          showConfirmButton: false,
          timer: 3000,
        });
        setSubmitting(false);
      }
    } catch (error) {
      console.log(error);
      setSubmitting(false);
    }
  }

  const formik = useFormik({
    initialValues: {
      full_name: '',
      phone: '',
      business_name: '',
      country: '',
      email: '',
      volume: '',
      message: '',
    },
    onSubmit(values, { setSubmitting, resetForm }) {
      setSubmitting(true);
      setTimeout(() => {
        resetForm();
        setSubmitting(false);
      }, 1500);
      sendContactUsFormData(values, setSubmitting);
      console.log(values);
    },
    validationSchema: Yup.object().shape({
      full_name: Yup.string().required('Please provide your full name'),
      phone: Yup.string().required('Input field is required'),
      business_name: Yup.string().required('Please provide your business full name'),
      country: Yup.string().required('Please select a country'),
      email: Yup.string().email('Invalid email').required('Please provide a valid business email'),
      volume: Yup.number()
        .positive('Invalid amount')
        .required('Please provide an amount')
        .integer(),
      message: Yup.string().required('Please drop us a message'),
    }),
  });

  return (
    <>
      <main>
        <div className="layer-blur">
          <div className="nav-wrapper">
            <nav className="navbar navbar-expand-lg align-items-center navbar-light">
              <div className="container-fluid">
                <Link to="/" className="logo">
                  <p className="logo-text">MiraPay</p>
                </Link>
                {/* Hamburger button */}
                <button
                  type="button"
                  className="navbar-toggler"
                  data-bs-target="#offcanvasNavbar"
                  data-bs-toggle="offcanvas"
                >
                  <span className="navbar-toggler-icon" />
                </button>

                <div
                  className="offcanvas offcanvas-end"
                  tabIndex="-1"
                  id="offcanvasNavbar"
                  aria-labelledby="offcanvasNavbarLabel"
                >
                  <div className="offcanvas-header">
                    <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                      MiraPay
                    </h5>
                    <button
                      type="button"
                      className="btn-close text-reset"
                      data-bs-dismiss="offcanvas"
                      aria-label="Close"
                    />
                  </div>

                  <div className="offcanvas-body align-items-center justify-content-between">
                    <ul className="navbar-nav mx-auto nav_list" id="navbar">
                      <li className="nav-item">
                        <NavLink activeClassName="active" exact to="/" className="nav-link mx-md-2">
                          Home
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                          About Us
                        </NavLink>
                      </li>
                      <li className="nav-item dropdown">
                        <Link
                          to="#"
                          className="nav-link dropdown-toggle mx-md-2"
                          role="button"
                          data-bs-toggle="dropdown"
                        >
                          Products
                        </Link>
                        <ul className="dropdown-menu">
                          <li>
                            <Link className="dropdown-item" to="/payment">
                              Payment Links
                            </Link>
                          </li>
                        </ul>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/testimonial"
                          className="nav-link mx-md-2"
                        >
                          Testimonials
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/pricing"
                          className="nav-link mx-md-2"
                        >
                          Pricing
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/contact"
                          className="nav-link mx-md-2"
                        >
                          Contact Us
                        </NavLink>
                      </li>
                    </ul>

                    <div className="my-auto">
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                          Sign In
                        </button>
                      </a>
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-1 btn-primary my-0 py-md-2">
                          Sign Up For Free
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>

          <section className="hero contact-hero py-3">
            <div className="contact my-5 py-md-5">
              <h1 className="contact-header text-center mx-auto my-3">Contact Us</h1>
              <p className="text-center contact-text">
                We’d love to hear more about your business, and how we can best serve you.
              </p>
            </div>
            <div className="card form-card shadow-lg border">
              <form className="form d-block" onSubmit={formik.handleSubmit}>
                <div className="p-5">
                  <div className="row my-md-3">
                    <div className="col-md-6">
                      <div className="form__form-group">
                        <label htmlFor="full_name" className="form__form-group-label">
                          Full Name
                        </label>
                        <input
                          type="text"
                          name="full_name"
                          id="full_name"
                          placeholder="Micheal Ola"
                          data-test="full-name"
                          value={formik.values.full_name}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        {formik.errors.full_name && formik.touched.full_name && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.full_name}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form__form-group">
                        <label htmlFor="phone" className="form__form-group-label">
                          Phone Number
                        </label>
                        <input
                          type="tel"
                          name="phone"
                          id="phone"
                          placeholder="+234 903 956 1875"
                          data-test="phone-number"
                          value={formik.values.phone}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        {formik.errors.phone && formik.touched.phone && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.phone}
                          </span>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row my-md-3">
                    <div className="col-md-6">
                      <div className="form__form-group">
                        <label htmlFor="business_name" className="form__form-group-label">
                          Business Name
                        </label>
                        <input
                          type="text"
                          name="business_name"
                          id="business_name"
                          data-test="business-name"
                          placeholder="Micheal Ventures"
                          value={formik.values.business_name}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        {formik.errors.business_name && formik.touched.business_name && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.business_name}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form__form-group-select">
                        <label htmlFor="country" className="form__form-group-label">
                          Select Country
                        </label>
                        <select
                          name="country"
                          data-test="country" 
                          value={formik.values.country}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          className="Select-input Select-control"
                        >
                          <option value="">Select a country</option>
                          <option value="Nigeria">Nigeria</option>
                          <option value="Ghana">Ghana</option>
                          <option value="South Africa">South Africa</option>
                          <option value="Other">Others</option>
                        </select>
                        {formik.errors.country && formik.touched.country && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.country}
                          </span>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row my-3">
                    <div className="col-md-6">
                      <div className="form__form-group">
                        <label htmlFor="email" className="form__form-group-label">
                          Business Email
                        </label>
                        <input
                          type="email"
                          name="email"
                          className="form-control"
                          id="email"
                          data-test="business-email"
                          placeholder="mirola@gmail.com"
                          value={formik.values.email}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        {formik.errors.email && formik.touched.email && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.email}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form__form-group">
                        <label htmlFor="volume" className="form__form-group-label">
                          Payment Volume
                        </label>
                        <CurrencyInput
                          id="volume"
                          name="volume"
                          placeholder="₦ 50,000"
                          data-test="payment-amount"
                          className="form-control"
                          prefix="&#8358;"
                          decimalsLimit={2}
                          value={formik.values.volume}
                          onBlur={formik.handleBlur}
                          onValueChange={(value) => {
                            // setNominal(value)
                            formik.setFieldValue('volume', value);
                          }}
                        />
                        {formik.errors.volume && formik.touched.volume && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.volume}
                          </span>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-md-3">
                    <div className="col-md-12">
                      <div className="form__form-group">
                        <label htmlFor="message" className="form-label">
                          Message
                        </label>
                        <textarea
                          name="message"
                          id="message"
                          data-test="message-input"
                          cols="30"
                          rows="5"
                          placeholder="Leave us a message"
                          value={formik.values.message}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                        {formik.errors.message && formik.touched.message && (
                          <span className="error-text form__form-group-error">
                            {formik.errors.message}
                          </span>
                        )}
                      </div>
                    </div>
                  </div>
                  <button
                    type="submit"
                    data-test="send-button"
                    disabled={formik.isSubmitting}
                    className="btn btn-primary my-3 float-end px-md-5"
                  >
                    {formik.isSubmitting ? (
                      <div className="d-flex align-items-center justify-content-center">
                        <Spinner radius={20} color={'#333'} stroke={2} visible={true} />
                      </div>
                    ) : (
                      'Send'
                    )}
                    {console.log(formik.isSubmitting)}
                  </button>
                </div>
              </form>
            </div>
          </section>
        </div>

        <footer className="footer my-5 py-md-5">
          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">Start accepting payments now!</h2>

                    <p className="question-text mt-0 py-3">
                      Do you want to get paid now? Do you want to start accepting payments in less
                      than 30 minutes? You are in the right place. Get started now!
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <a className="sitemap-contact m-0" href="mailto:mirapaymentsltd@gmail.com">
                    mirapaymentsltd@gmail.com
                  </a>
                  <p>
                    <a className="sitemap-contact" href="tel:+2347069179050">
                      +234 7069179050
                    </a>
                  </p>
                  <p className="sitemap-contact">Lagos, Nigeria</p>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quick Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/testimonial">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/payment">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Terms & Condition</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

export default ContactPage;
