import React from "react";
import hero from "./svgs/hero_image.svg";

const Onlinepayment = () => {
  return (
    <div className="onlinepayment pt-3 mt-3">
      <div className="container-fluid my-3">
        <div className="row">
          <div className="col-md-6 my-md-auto">
            <header>
              <h1 className="onlinepayment-header">
                Easy online payment gateway for everyone
              </h1>
              <p className="onlinepayment-text my-4">
                <span className="font-weight-bold">MiraPay</span> allows
                different variety of people (Individuals, NGOs, Government,
                Religious Bodies etc) to receive payments and also make payments
                accessible from every part of the world for everyone.
              </p>
              <button className="btn btn-primary">Sign Up for Free</button>
              <button className="btn btn-light btn-contact">
                Contact Sales
              </button>
            </header>
          </div>
          <div className="col-md-6 my-md-auto">
            <img className="img-fluid" src={hero} alt="Hero" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Onlinepayment;
