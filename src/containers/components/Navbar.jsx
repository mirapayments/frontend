import React from "react";
import { Link, NavLink } from "react-router-dom";


const Navbar = () => {
    return (
    <nav className="navbar navbar-expand-lg align-items-center navbar-light">
        <div className="container-fluid">
            <Link to="/home" className="logo">
            <p className="logo-text">MiraPay</p>
            </Link>
            {/* Hamburger button */}
            <button
            type="button"
            className="navbar-toggler"
            data-bs-target="#offcanvasNavbar"
            data-bs-toggle="offcanvas"
            >
            <span className="navbar-toggler-icon"></span>
            </button>

            <div
            className="offcanvas offcanvas-end"
            tabIndex="-1"
            id="offcanvasNavbar"
            aria-labelledby="offcanvasNavbarLabel"
            >
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasNavbarLabel"></h5>
                <button
                type="button"
                class="btn-close text-reset"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
                ></button>
            </div>

            <div className="offcanvas-body align-items-center justify-content-between">
                <ul className="navbar-nav mx-auto nav_list" id="navbar">
                <li className="nav-item">
                    <NavLink
                    activeClassName="active"
                    to="/home"
                    className="nav-link mx-md-2"
                    >
                    Home
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                    activeClassName="active"
                    to="/about"
                    className="nav-link mx-md-2"
                    >
                    About Us
                    </NavLink>
                </li>
                <li className="nav-item dropdown">
                    <Link
                    to="#"
                    className="nav-link dropdown-toggle mx-md-2"
                    role="button"
                    data-bs-toggle="dropdown"
                    >
                    Products
                    </Link>
                    <ul className="dropdown-menu">
                    <li>
                        <Link className="dropdown-item" to="/payment">
                        Payment Links
                        </Link>
                    </li>
                    </ul>
                </li>
                <li className="nav-item">
                    <NavLink
                    activeClassName="active"
                    to="/testimonial"
                    className="nav-link mx-md-2"
                    >
                    Testimonials
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                    activeClassName="active"
                    to="/pricing"
                    className="nav-link mx-md-2"
                    >
                    Pricing
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                    activeClassName="active"
                    to="/contact"
                    className="nav-link mx-md-2"
                    >
                    Contact Us
                    </NavLink>
                </li>
                </ul>

                <div className="my-auto">
                <a
                    href="https://tinyurl.com/mirapaywaitinglink"
                    target="_blank"
                    rel="noreferrer"
                >
                    <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                    Sign In
                    </button>
                </a>
                <a
                    href="https://tinyurl.com/mirapaywaitinglink"
                    target="_blank"
                    rel="noreferrer"
                >
                    <button className="btn border-1 btn-primary my-0 py-md-2">
                    Sign Up For Free
                    </button>
                </a>
                </div>
            </div>
            </div>
        </div>
    </nav>
    )
}

export default Navbar;
