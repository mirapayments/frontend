import React from "react";
import { Route, Switch } from "react-router-dom";
import MainWrapper from "./MainWrapper";
import Home from "../Home/Home";
import NotFound from "../DefaultPage/404/index";
import Paymentpage from "../Paymentpage";
import Homepage from "../Home/Homepage";
import AboutPage from "../AboutPage/AboutPage";
import ProductPage from "../ProductPage/ProductPage";
import PricingPage from "../PricingPage/PricingPage";
import ContactPage from "../ContactPage/ContactPage";
import TestimonialPage from "../TestimonialPage/TestimonialPage";
import PrivacyPage from "../PrivacyPage/PrivacyPage";
import TransactionStatusPage from "../TransactionStatusPage/TransactionStatusPage";
import Pay from "../Pay";

const Router = () => {
  return (
    <MainWrapper>
      <main>
        <Switch>
          <Route exact path="/404" component={NotFound} />
          <Route exact path="/" component={Homepage} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/about" component={AboutPage} />
          <Route exact path="/payment" component={ProductPage} />
          <Route exact path="/pay" component={Pay} />
          <Route exact path="/pay/:payid" component={Paymentpage} />
          <Route exact path="/pricing" component={PricingPage} />
          <Route exact path="/contact" component={ContactPage} />
          <Route exact path="/testimonial" component={TestimonialPage} />
          <Route exact path="/privacy" component={PrivacyPage} />
          <Route exact path="/transaction/status" component={TransactionStatusPage} />
          <Route component={NotFound} />
        </Switch>
      </main>
    </MainWrapper>
  );
};

export default Router;
