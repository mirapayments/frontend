// Import the necessary libraries and modules.
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import '../Home/Pages.css';
import checker from '../Home/svgs/checker.svg';
import guard from '../Home/gifs/security-pay.gif';
import card from '../Home/svgs/card.svg';
import code from '../Home/svgs/qr-code.svg';
import bankTerminal from '../Home/svgs/bank-terminal.svg';
import balance from '../Home/svgs/account-balance.svg';
import smarttouch from '../Home/svgs/smartphone-touch-screen.svg';
import bankCard from '../Home/svgs/bank-card.svg';

// Pricing Page Component
const PricingPage = () => {
  return (
    <>
      <main>
        <div className="layer-blur">
          <div className="nav-wrapper">
            <nav className="navbar navbar-expand-lg align-items-center navbar-light">
              <div className="container-fluid">
                <Link to="/" className="logo">
                  <p className="logo-text">MiraPay</p>
                </Link>
                {/* Hamburger button */}
                <button
                  type="button"
                  className="navbar-toggler"
                  data-bs-target="#offcanvasNavbar"
                  data-bs-toggle="offcanvas"
                >
                  <span className="navbar-toggler-icon" />
                </button>

                <div
                  className="offcanvas offcanvas-end"
                  tabIndex="-1"
                  id="offcanvasNavbar"
                  aria-labelledby="offcanvasNavbarLabel"
                >
                  <div className="offcanvas-header">
                    <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                      MiraPay
                    </h5>
                    <button
                      type="button"
                      className="btn-close text-reset"
                      data-bs-dismiss="offcanvas"
                      aria-label="Close"
                    />
                  </div>

                  <div className="offcanvas-body align-items-center justify-content-between">
                    <ul className="navbar-nav mx-auto nav_list" id="navbar">
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/" exact className="nav-link mx-md-2">
                          Home
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                          About Us
                        </NavLink>
                      </li>
                      <li className="nav-item dropdown">
                        <Link
                          to="#"
                          className="nav-link dropdown-toggle mx-md-2"
                          role="button"
                          data-bs-toggle="dropdown"
                        >
                          Products
                        </Link>
                        <ul className="dropdown-menu">
                          <li>
                            <Link className="dropdown-item" to="/payment">
                              Payment Links
                            </Link>
                          </li>
                        </ul>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/testimonial"
                          className="nav-link mx-md-2"
                        >
                          Testimonials
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/pricing"
                          className="nav-link mx-md-2"
                        >
                          Pricing
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/contact"
                          className="nav-link mx-md-2"
                        >
                          Contact Us
                        </NavLink>
                      </li>
                    </ul>

                    <div className="my-auto">
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                          Sign In
                        </button>
                      </a>
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-1 btn-primary my-0 py-md-2">
                          Sign Up For Free
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>

          <section className="hero pricing-hero py-3">
            <div className="pricing my-5 py-md-5">
              <h1 className="pricing-header text-center mx-auto my-3">
                Simple, transparent and affordable pricing for business of all sizes
              </h1>
              <div className="container-fluid mt-5 pt-md-5">
                <div className="row">
                  <div className="col-md-6 my-3">
                    <div className="card text-center local-card shadow-lg border">
                      <div className="card-header bg-transparent my-md-3 pt-3">
                        Local Transactions
                      </div>
                      <div className="card-body">
                        <p className="body-content">
                          Access a complete payments platform with simple, pay-as-you-go pricing
                        </p>
                        <p className="content-title pt-2">1.3% + NGN 100</p>
                      </div>
                      <div className="card-footer bg-transparent">
                        <ul className="card-lists px-auto">
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>₦100 fee waived for transactions under ₦2000</span>
                          </li>
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>
                              Local transactions fees are capped at ₦2500, meaning that's the
                              absolute maximum you'll ever pay in fees per transaction
                            </span>
                          </li>
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>Get hundreds of feature updates each year</span>
                          </li>
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>No setup fees, monthly fees, or hidden fees</span>
                          </li>
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>No maintenance fees</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 my-3">
                    <div className="card text-center international-card border shadow-lg">
                      <div className="card-header my-md-3 pt-3">International Transactions</div>
                      <div className="card-body">
                        <p className="body-content text-white">
                          Access a complete payments platform with simple, pay-as-you-go pricing
                        </p>
                        <p className="content-title text-white pt-2">3.7%</p>
                      </div>
                      <div className="card-footer">
                        <ul className="card-lists text-start">
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>Accept payments from around the world</span>
                          </li>
                          <li className="py-2 list d-flex align-items-start text-start">
                            <img className="img-fluid checker pe-2" src={checker} alt="checker" />
                            <span>
                              International cards are charged and settled in your local currency by
                              default
                            </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        {/* <section className="faq my-5 py-md-5 my-3">
          <div className="faq-section py-3">
            <div className="container-fluid my-md-3 py-md-3">
              <div className="row">
                <div className="col-md-5">
                  <h2 className="faq-header">
                    Frequently Asked Questions (FAQs)
                  </h2>
                </div>
                <div className="col-md-7">
                  <div className="question-answer mt-3">
                    <h3 className="question">When do I receive my money?</h3>
                    <p className="answer">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Placerat eget massa, sed suspendisse euismod ipsum
                      fermentum sit. Blandit vitae, nulla egestas malesuada
                      urna. Sagittis at urna morbi sem dis. Nisl ante diam
                      pharetra tempor, ultrices turpis platea sed purus.
                    </p>
                  </div>
                  <div className="question-answer my-3">
                    <h3 className="question">
                      Are there any transaction limits?
                    </h3>
                    <p className="answer">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Placerat eget massa, sed suspendisse euismod ipsum
                      fermentum sit. Blandit vitae, nulla egestas malesuada
                      urna. Sagittis at urna morbi sem dis. Nisl ante diam
                      pharetra tempor, ultrices turpis platea sed purus.
                    </p>
                  </div>
                  <div className="question-answer my-3">
                    <h3 className="question">
                      Who bear the transaction charge?
                    </h3>
                    <p className="answer">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Placerat eget massa, sed suspendisse euismod ipsum
                      fermentum sit. Blandit vitae, nulla egestas malesuada
                      urna. Sagittis at urna morbi sem dis. Nisl ante diam
                      pharetra tempor, ultrices turpis platea sed purus.
                    </p>
                  </div>
                  <div className="question-answer my-3">
                    <h3 className="question">
                      Do you offer transaction volume discounts?
                    </h3>
                    <p className="answer">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Placerat eget massa, sed suspendisse euismod ipsum
                      fermentum sit. Blandit vitae, nulla egestas malesuada
                      urna. Sagittis at urna morbi sem dis. Nisl ante diam
                      pharetra tempor, ultrices turpis platea sed purus.
                    </p>
                  </div>
                  <div className="question-answer mb-3">
                    <h3 className="question">
                      Are you able to offer flat rate pricing for schools?
                    </h3>
                    <p className="answer">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Placerat eget massa, sed suspendisse euismod ipsum
                      fermentum sit. Blandit vitae, nulla egestas malesuada
                      urna. Sagittis at urna morbi sem dis. Nisl ante diam
                      pharetra tempor, ultrices turpis platea sed purus.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section> */}

        <section className="services my-md-5 py-md-5">
          <h2 className="service-header text-center my-md-3 py-md-3">Our Services</h2>
          <p className="our-service">
            We offer new generation online payments to help you scale your business in a very short
            time. We support a wide variety of payment channels, so you wouldn't have to worry about
            not receiving payments from a set group of individuals.
          </p>
          <div className="container-fluid">
            <div className="row my-3 py-3">
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={card} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Card</h5>
                  <p className="service-text my-3">
                    We process card payments in a fast and secure way. Card payments have never been
                    this seamless
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={bankCard} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Bank Account</h5>
                  <p className="service-text my-3">
                    Choose your bank among the supported banks and begin payments
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={smarttouch} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Bank Transfer</h5>
                  <p className="service-text my-3">
                    Users can pay using our automatically generated bank accounts
                  </p>
                </div>
              </div>
            </div>

            <div className="row mb-3 pb-3">
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={balance} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">USSD</h5>
                  <p className="service-text my-3">
                    Users can make payments from any mobile device with our USSD payment platform
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={bankTerminal} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">POS(coming soon)</h5>
                  <p className="service-text my-3">
                    In a short time, users can make payments using point of sale devices
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={code} alt="QR Code" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Visa QR (coming soon)</h5>
                  <p className="service-text my-3">
                    In a short while, you’ll just have to scan a code to make payments
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <footer className="footer my-md-5 py-md-5">
          <section className="security-section">
            <div className="row">
              <div className="col-md-6">
                <h2 className="security-header">Guaranteed Security and Safety</h2>
                <div className="my-3">
                  <p className="security-title">Your business is properly secured</p>
                  <p className="security-text">
                    We have put infrastructures in place to help your stay secure in the midst of
                    different level of fraudulent activity all over the internet
                  </p>
                </div>

                <div className="my-md-3">
                  <p className="security-title">
                    You and your customers are protected with advanced fraud detection
                  </p>
                  <p className="security-text">
                    All payments and transactions are secured end to end with our high security
                    infrastructure. We have no tolerance for fraud
                  </p>
                </div>
              </div>

              <div className="col-md-6 guide-image">
                <img className="img-fluid" src={guard} alt="guard" />
              </div>
            </div>
          </section>

          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">Start accepting payments now!</h2>

                    <p className="question-text mt-0 py-3">
                      Do you want to get paid now? Do you want to start accepting payments in less
                      than 30 minutes? You are in the right place. Get started now!
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <a className="sitemap-contact m-0" href="mailto:mirapaymentsltd@gmail.com">
                    mirapaymentsltd@gmail.com
                  </a>
                  <p>
                    <a className="sitemap-contact" href="tel:+2347069179050">
                      +234 7069179050
                    </a>
                  </p>
                  <p className="sitemap-contact">Lagos, Nigeria</p>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quick Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/testimonial">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/payment">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Terms & Condition</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

// Export Component
export default PricingPage;
