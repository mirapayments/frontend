// Import the necessary libraries and modules.
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import '../Home/Pages.css';
import hero from './webp/Hero.webp';

// Testimonial page component.
const TestimonialPage = () => {
  return (
    <>
      <main>
        <div className="testimonial-wrapper py-3">
          <div className="nav-wrapper">
            <nav className="navbar navbar-expand-lg align-items-center navbar-light">
              <div className="container-fluid">
                <Link to="/" className="logo">
                  <p className="logo-text">MiraPay</p>
                </Link>
                {/* Hamburger button */}
                <button
                  type="button"
                  className="navbar-toggler"
                  data-bs-target="#offcanvasNavbar"
                  data-bs-toggle="offcanvas"
                >
                  <span className="navbar-toggler-icon" />
                </button>

                <div
                  className="offcanvas offcanvas-end"
                  tabIndex="-1"
                  id="offcanvasNavbar"
                  aria-labelledby="offcanvasNavbarLabel"
                >
                  <div className="offcanvas-header">
                    <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                      MiraPay
                    </h5>
                    <button
                      type="button"
                      className="btn-close text-reset"
                      data-bs-dismiss="offcanvas"
                      aria-label="Close"
                    />
                  </div>

                  <div className="offcanvas-body align-items-center justify-content-between">
                    <ul className="navbar-nav mx-auto nav_list" id="navbar">
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/" exact className="nav-link mx-md-2">
                          Home
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                          About Us
                        </NavLink>
                      </li>
                      <li className="nav-item dropdown">
                        <Link
                          to="#"
                          className="nav-link dropdown-toggle mx-md-2"
                          role="button"
                          data-bs-toggle="dropdown"
                        >
                          Products
                        </Link>
                        <ul className="dropdown-menu">
                          <li>
                            <Link className="dropdown-item" to="/payment">
                              Payment Links
                            </Link>
                          </li>
                        </ul>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/testimonial"
                          className="nav-link mx-md-2"
                        >
                          Testimonials
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/pricing"
                          className="nav-link mx-md-2"
                        >
                          Pricing
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/contact"
                          className="nav-link mx-md-2"
                        >
                          Contact Us
                        </NavLink>
                      </li>
                    </ul>

                    <div className="my-auto">
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                          Sign In
                        </button>
                      </a>
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-1 btn-primary my-0 py-md-2">
                          Sign Up For Free
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>

          <section className="hero">
            <div className="testimonial my-3 py-3">
              <div className="container-fluid my-3">
                <div className="row">
                  <div className="col-md-6 my-md-auto">
                    <header>
                      <h1 className="testimonial-header">
                        What our customers say about us and our services
                      </h1>
                      <p className="testimonial-text my-3 pb-md-5">
                        Our customers have so much to say about us and the services we provide for
                        them.
                      </p>
                      <button className="btn btn-primary">Add your Story</button>
                    </header>
                  </div>
                  <div className="col-md-6 my-md-auto">
                    <img
                      loading="lazy"
                      className="img-fluid testimonial-hero"
                      src={hero}
                      alt="Hero"
                    />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        {/* <section className="customer-story my-md-5 py-md-5">
          <h2 className="customer-story-header text-center my-md-3 py-md-3">
            Verified Customer Stories
          </h2>
          <div className="container-fluid">
            <div className="row my-3 py-3">
              <div className="col-md-4 my-3">
                <div className="card customer shadow-sm p-0 border">
                  <div className="card-body p-3">
                    <img src={Logo1} alt="Mansard Logo" className="pb-3" />
                    <div className="card-text">
                      <p className="story-text my-3">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Elit, sed est quisque proin a aliquet viverra quam.
                        Commodo sapien morbi integer semper lorem nunc sed
                        quisque. Elementum in sed sed ultrices dolor. Platea
                        etiam porttitor tristique ultrices.
                      </p>
                    </div>
                    <div className="customer-detail d-flex align-items-center pt-3">
                      <div className="customer-image me-3">
                        <img src={Image1} alt="" />
                      </div>
                      <div className="customer-name-portfolio">
                        <p className="customer-name pb-1">Miracle Alex</p>
                        <span className="customer-portfolio">
                          CEO & Founder, Mirapay
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 my-3">
                <div className="card customer shadow-sm p-0 border">
                  <div className="card-body p-3">
                    <img src={Logo2} alt="Mansard Logo" className="pb-3" />
                    <div className="card-text">
                      <p className="story-text my-3">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Elit, sed est quisque proin a aliquet viverra quam.
                        Commodo sapien morbi integer semper lorem nunc sed
                        quisque. Elementum in sed sed ultrices dolor. Platea
                        etiam porttitor tristique ultrices.
                      </p>
                    </div>
                    <div className="customer-detail d-flex align-items-center pt-3">
                      <div className="customer-image me-3">
                        <img src={Image2} alt="" />
                      </div>
                      <div className="customer-name-portfolio">
                        <p className="customer-name pb-1">Miracle Alex</p>
                        <span className="customer-portfolio">
                          CEO & Founder, Mirapay
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 my-3">
                <div className="card customer shadow-sm p-0 border">
                  <div className="card-body p-3">
                    <img src={Logo3} alt="Mansard Logo" className="pb-3" />
                    <div className="card-text">
                      <p className="story-text my-3">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Elit, sed est quisque proin a aliquet viverra quam.
                        Commodo sapien morbi integer semper lorem nunc sed
                        quisque. Elementum in sed sed ultrices dolor. Platea
                        etiam porttitor tristique ultrices.
                      </p>
                    </div>
                    <div className="customer-detail d-flex align-items-center pt-3">
                      <div className="customer-image me-3">
                        <img src={Image3} alt="" />
                      </div>
                      <div className="customer-name-portfolio">
                        <p className="customer-name pb-1">Miracle Alex</p>
                        <span className="customer-portfolio">
                          CEO & Founder, Mirapay
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row mb-3 pb-3">
              <div className="col-md-4 my-3">
                <div className="card customer shadow-sm p-0 border">
                  <div className="card-body p-3">
                    <img src={Logo1} alt="Mansard Logo" className="pb-3" />
                    <div className="card-text">
                      <p className="story-text my-3">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Elit, sed est quisque proin a aliquet viverra quam.
                        Commodo sapien morbi integer semper lorem nunc sed
                        quisque. Elementum in sed sed ultrices dolor. Platea
                        etiam porttitor tristique ultrices.
                      </p>
                    </div>
                    <div className="customer-detail d-flex align-items-center pt-3">
                      <div className="customer-image me-3">
                        <img src={Image1} alt="" />
                      </div>
                      <div className="customer-name-portfolio">
                        <p className="customer-name pb-1">Miracle Alex</p>
                        <span className="customer-portfolio">
                          CEO & Founder, Mirapay
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 my-3">
                <div className="card customer shadow-sm p-0 border">
                  <div className="card-body p-3">
                    <img src={Logo1} alt="Mansard Logo" className="pb-3" />
                    <div className="card-text">
                      <p className="story-text my-3">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Elit, sed est quisque proin a aliquet viverra quam.
                        Commodo sapien morbi integer semper lorem nunc sed
                        quisque. Elementum in sed sed ultrices dolor. Platea
                        etiam porttitor tristique ultrices.
                      </p>
                    </div>
                    <div className="customer-detail d-flex align-items-center pt-3">
                      <div className="customer-image me-3">
                        <img src={Image1} alt="" />
                      </div>
                      <div className="customer-name-portfolio">
                        <p className="customer-name pb-1">Miracle Alex</p>
                        <span className="customer-portfolio">
                          CEO & Founder, Mirapay
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 my-3">
                <div className="card customer shadow-sm p-0 border">
                  <div className="card-body p-3">
                    <img src={Logo1} alt="Mansard Logo" className="pb-3" />
                    <div className="card-text">
                      <p className="story-text my-3">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Elit, sed est quisque proin a aliquet viverra quam.
                        Commodo sapien morbi integer semper lorem nunc sed
                        quisque. Elementum in sed sed ultrices dolor. Platea
                        etiam porttitor tristique ultrices.
                      </p>
                    </div>
                    <div className="customer-detail d-flex align-items-center pt-3">
                      <div className="customer-image me-3">
                        <img src={Image1} alt="" />
                      </div>
                      <div className="customer-name-portfolio">
                        <p className="customer-name pb-1">Miracle Alex</p>
                        <span className="customer-portfolio">
                          CEO & Founder, Mirapay
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section> */}

        <footer className="footer my-md-5 py-md-5">
          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">Start accepting payments now!</h2>

                    <p className="question-text mt-0 py-3">
                      Do you want to get paid now? Do you want to start accepting payments in less
                      than 30 minutes? You are in the right place. Get started now!
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <a className="sitemap-contact m-0" href="mailto:mirapaymentsltd@gmail.com">
                    mirapaymentsltd@gmail.com
                  </a>
                  <p>
                    <a className="sitemap-contact" href="tel:+2347069179050">
                      +234 7069179050
                    </a>
                  </p>
                  <p className="sitemap-contact">Lagos, Nigeria</p>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quicks Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/home">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/testimonial">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/payment">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-terms m-0">Terms & Condition</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

// Export the component
export default TestimonialPage;
