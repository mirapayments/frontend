// Import the necessary libraries and modules.
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import '../Home/Pages.css';
import hero from './webp/hero.webp';
import mission from './webp/mission.webp';
import Vision from './webp/vision.webp';
import guard from '../Home/gifs/security-pay.gif';

// About page Component

const AboutPage = () => {
  return (
    <>
      <main>
        <div className="nav-wrapper">
          <nav className="navbar navbar-expand-lg align-items-center navbar-light">
            <div className="container-fluid">
              <Link to="/" className="logo">
                <p className="logo-text">MiraPay</p>
              </Link>
              {/* Hamburger button */}
              <button
                type="button"
                className="navbar-toggler"
                data-bs-target="#offcanvasNavbar"
                data-bs-toggle="offcanvas"
              >
                <span className="navbar-toggler-icon" />
              </button>

              <div
                className="offcanvas offcanvas-end"
                tabIndex="-1"
                id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel"
              >
                <div className="offcanvas-header">
                  <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                    MiraPay
                  </h5>
                  <button
                    type="button"
                    className="btn-close text-reset"
                    data-bs-dismiss="offcanvas"
                    aria-label="Close"
                  />
                </div>

                <div className="offcanvas-body align-items-center justify-content-between">
                  <ul className="navbar-nav mx-auto nav_list" id="navbar">
                    <li className="nav-item">
                      <NavLink exact activeClassName="active" to="/" className="nav-link mx-md-2">
                        Home
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                        About Us
                      </NavLink>
                    </li>
                    <li className="nav-item dropdown">
                      <Link
                        to="#"
                        className="nav-link dropdown-toggle mx-md-2"
                        role="button"
                        data-bs-toggle="dropdown"
                      >
                        Products
                      </Link>
                      <ul className="dropdown-menu">
                        <li>
                          <Link className="dropdown-item" to="/payment">
                            Payment Links
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item">
                      <NavLink
                        activeClassName="active"
                        to="/testimonial"
                        className="nav-link mx-md-2"
                      >
                        Testimonials
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink activeClassName="active" to="/pricing" className="nav-link mx-md-2">
                        Pricing
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink activeClassName="active" to="/contact" className="nav-link mx-md-2">
                        Contact Us
                      </NavLink>
                    </li>
                  </ul>

                  <div className="my-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                        Sign In
                      </button>
                    </a>
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn border-1 btn-primary my-0 py-md-2">
                        Sign Up For Free
                      </button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
        <section className="hero py-3">
          <div className="onlinepayment mt-3 pt-3">
            <div className="container-fluid my-3">
              <div className="row">
                <div className="col-md-6 my-md-auto">
                  <h1 className="hero-header">
                    A passionate team building economic infrastructure through a great payment
                    gateway
                  </h1>
                  {/* <p className="hero-text my-4">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Mattis eu, arcu in euismod. Ullamcorper fringilla posuere
                    malesuada amet. Vitae, egestas donec odio et est leo quis.
                    Pharetra netus non, metus, facilisis nunc nec, sit.
                  </p> */}
                </div>
                <div className="col-md-6 my-md-auto">
                  <img loading="lazy" className="img-fluid" src={hero} alt="Hero" />
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="mission my-md-5 py-md-5">
          <div className="container-fluid">
            <div className="row mission-row g-0 mt-md-5">
              <div className="col-md-6">
                <img loading="lazy" className="img-fluid" src={mission} alt="Mission" />
              </div>

              <div className="col-md-6 p-0 my-lg-auto">
                <h3 className="mission-header my-3">Our Mission</h3>
                <p className="mission-text my-3">
                  We intend to lower the barrier of receiving payments from anywhere in the world.
                  Our goal in Mirapayments is to reduce the cost of online transactions, increase
                  availability to a wider variety of users, increase transaction success rates and
                  good customer relations. We want to keep everyone happy, save and fulfilled after
                  an online transaction
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className="vision my-md-5 py-md-5">
          <div className="container-fluid">
            <div className="row g-0 mt-md-5">
              <div className="col-md-6 ps-md-3 my-lg-auto">
                <h3 className="vision-header my-3">Our Vision</h3>
                <p className="vision-text my-3">
                  We seek to foster economic growth, increase the availability of secured and
                  reliable payments by providing an application and infrastructure that facilitates
                  payments processing for large, medium and small businesses, online retailers,
                  governmental organizations, non governmental organizations and religious bodies,
                  We want to keep everyone happy, save and fulfilled after an online transaction.
                </p>
              </div>

              <div className="col-md-6">
                <img loading="lazy" className="img-fluid" src={Vision} alt="Vision" />
              </div>
            </div>
          </div>
        </section>
        <section className="customers my-md-5 py-md-5">
          <h1 className="text-center customers-header mx-auto my-3 pb-md-3">
            We are here to serve your needs
          </h1>
          <div className="timeline">
            <div className="timeline-container right">
              <div className="timeline-content">
                <h3 className="timeline-header">Safe and Secure Transactions</h3>
                <p className="timeline-text">
                  We help protect your business and payments from fraud and increase success rates
                  for transactions while using our platform.
                </p>
              </div>
            </div>
            <div className="timeline-container left">
              <div className="timeline-content">
                <h3 className="timeline-header">Get Paid Anywhere</h3>
                <p className="timeline-text">
                  Everything you need to accept payments, we have it all ready for you. Our platform
                  has been optimized for different payment options.
                </p>
              </div>
            </div>
            <div className="timeline-container right">
              <div className="timeline-content">
                <h3 className="timeline-header">Great User Experience</h3>
                <p className="timeline-text">
                  At Mirapay, we are building a user-friendly platform with great functionalities to
                  help our users navigate through seamlessly.
                </p>
              </div>
            </div>
            <div className="timeline-container left">
              <div className="timeline-content">
                <h3 className="timeline-header">Strong and Robust Platform</h3>
                <p className="timeline-text">
                  We have built a strong infrastructure capable of handling your transactions from
                  thousands to millions with a smooth experience.
                </p>
              </div>
            </div>
          </div>
        </section>
        {/* <section className="our-team my-md-5 py-md-5">
          <h1 className="our-team-header text-center my-3 pb-md-3">Our Team</h1>
          <div className="container-fluid py-md-4">
            <div className="row teams align-content-between">
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture1} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture2} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture3} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture4} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture3} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture4} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture1} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture2} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture3} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture4} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture3} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
              <div className="col-md-2 my-3">
                <div className="members text-center">
                  <img src={Picture4} alt="Team Member" />
                  <p className="name">Agbabiaka Lataro</p>
                  <span className="portfolio">Founder</span>
                </div>
              </div>
            </div>

            <div
              className="carousel slide"
              id="teamCarousel"
              data-bs-ride="carousel"
            >
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <div className="members text-center d-block w-100">
                    <img src={Picture1} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture2} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture3} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture4} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture3} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture4} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture1} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture2} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture3} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture4} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture3} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
                <div className="carousel-item">
                  <div className="members text-center d-block w-100">
                    <img src={Picture4} alt="Team Member" />
                    <p className="name">Agbabiaka Lataro</p>
                    <span className="portfolio">Founder</span>
                  </div>
                </div>
              </div>

              <button
                class="carousel-control-prev"
                type="button"
                data-bs-target="#teamCarousel"
                data-bs-slide="prev"
              >
                <span
                  class="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button
                class="carousel-control-next"
                type="button"
                data-bs-target="#teamCarousel"
                data-bs-slide="next"
              >
                <span
                  class="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </section> */}
        <footer className="footer my-md-5 py-md-5">
          <section className="security-section">
            <div className="row">
              <div className="col-md-6">
                <h2 className="security-header">Guaranteed Security and Safety</h2>
                <div className="my-3">
                  <p className="security-title">Your business is properly secured</p>
                  <p className="security-text">
                    We have put infrastructures in place to help your stay secure in the midst of
                    different level of fraudulent activity all over the internet
                  </p>
                </div>

                <div className="my-md-3">
                  <p className="security-title">
                    You and your customers are protected with advanced fraud detection
                  </p>
                  <p className="security-text">
                    All payments and transactions are secured end to end with our high security
                    infrastructure. We have no tolerance for fraud
                  </p>
                </div>
              </div>

              <div className="col-md-6 guide-image">
                <img loading="lazy" className="img-fluid" src={guard} alt="guard" />
              </div>
            </div>
          </section>

          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">Start accepting payments now!</h2>

                    <p className="question-text mt-0 py-3">
                      Do you want to get paid now? Do you want to start accepting payments in less
                      than 30 minutes? You are in the right place. Get started now!
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <a className="sitemap-contact m-0" href="mailto:mirapaymentsltd@gmail.com">
                    mirapaymentsltd@gmail.com
                  </a>
                  <p>
                    <a className="sitemap-contact" href="tel:+2347069179050">
                      +234 7069179050
                    </a>
                  </p>
                  <p className="sitemap-contact">Lagos, Nigeria</p>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quick Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/testimonial">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/payment">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Terms & Condition</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

export default AboutPage;
