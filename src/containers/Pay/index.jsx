// Import the necessary libraries and modules.
import React, { useState } from 'react';
import PayForm from './payForm';


// Checkout Form
const Pay = () => {
    const [cardList, setCardList] = useState([]);

    return (
        <PayForm setCardList={setCardList} cardList={cardList} />
    );
}


// Export component
export default Pay;