import React, { useReducer, useState, useMemo } from "react";
import { Alert } from "reactstrap";
import { useSelector } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";
import styles from "./pay.module.css";
import companyLogo from "../../shared//img/mirapaylogo.png";
import padlock from "../../shared//img/padlock.svg";
import MaskedInput from "react-text-mask";
import {
  AMERICANEXPRESS,
  OTHERCARDS,
  EXPIRYDATE,
  CVV,
  CARDARR,
  CARDICON,
} from "./constant";
import {
  cardNumberValidation,
  cardExpiryValidation,
  textWithSpacesOnly,
  minLength,
} from "./validations";
import { createTransaction } from "../../utils/apiHandlers/createTransaction";
import moment from "moment";

// Reducer Function
const reducer = (state, action) => {
  switch (action.type) {
    case "card":
      return { ...state, card: action.data };
    case "expiry":
      return { ...state, expiry: action.data };
    case "cvv":
      return { ...state, cvv: action.data };
    case "cardHolder":
      return { ...state, cardHolder: action.data };
    case "cleanState":
      return { ...action.data };
    default:
      return state;
  }
};

function findDebitCardType(cardNumber) {
  const regexPattern = {
    MASTERCARD: /^5[1-5][0-9]{1,}|^2[2-7][0-9]{1,}$/,
    VISA: /^4[0-9]{2,}$/,
    AMERICAN_EXPRESS: /^3[47][0-9]{5,}$/,
    DISCOVER: /^6(?:011|5[0-9]{2})[0-9]{3,}$/,
    DINERS_CLUB: /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/,
    JCB: /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/,
  };

  for (const card in regexPattern) {
    if (cardNumber.replace(/[^\d]/g, "").match(regexPattern[card])) return card;
  }
  return "";
}

// Form
const initialState = { card: "", expiry: "", cvv: "", cardHolder: "" };

const PayForm = (props) => {
  const [error, setError] = useState({});
  const [cardType, setCardType] = useState();
  let { paymentLink } = useSelector((state) => state.ui);
  const [state, dispatch] = useReducer(reducer, initialState);
  const [failed, setFailed] = useState(null);
  const [loading, setLoading] = useState(false);
  let history = useHistory();

  function useSearchQuery() {
    const { search } = useLocation();
    return useMemo(() => new URLSearchParams(search), [search]);
  }

  let searchQuery = useSearchQuery();
  let email = atob(searchQuery?.get("email"));

  const handleValidations = (type, value) => {
    let errorText;
    switch (type) {
      case "card":
        setCardType(findDebitCardType(value));
        errorText = cardNumberValidation(value);
        setError({ ...error, cardError: errorText });
        break;
      case "cardHolder":
        errorText = value === "" ? "Required" : textWithSpacesOnly(value);
        setError({ ...error, cardHolderError: errorText });
        break;
      case "expiry":
        errorText = value === "" ? "Required" : cardExpiryValidation(value);
        setError({ ...error, expiryError: errorText });
        break;
      case "cvv":
        errorText = value === "" ? "Required" : minLength(3)(value);
        setError({ ...error, cvvError: errorText });
        break;
      default:
        break;
    }
  };

  const handleInputData = (e) => {
    dispatch({ type: e.target.name, data: e.target.value });
  };

  const handleBlur = (e) => {
    handleValidations(e.target.name, e.target.value);
  };

  const checkErrorBeforeSubmit = () => {
    let errorValue = error;
    let isError = false;

    Object.keys(error).forEach(async (val) => {
      if (state[val] === "") {
        isError = false;
      }
    });

    Object.keys(state).forEach(async (val) => {
      if (state[val] === "") {
        errorValue = { ...errorValue, [`${val + "Error"}`]: "Required" };
        setError(errorValue);
        isError = true;
      }
    });
    return isError;
  };

  const cleanData = { card: "", expiry: "", cvv: "", cardHolder: "" };

  function formatCardType(string) {
    if (string === "VISA") return "Visa";
    if (string === "AMERICAN_EXPRESS") return "American Express";
    if (string === "MASTER_CARD") return "Master Card";
    if (string === "DISCOVER") return "Discover";
    if (string === "DINERS_CLUB") return "Diners Club";
    if (string === "JCB") return "Jcb";
    return "";
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    let errorCheck = checkErrorBeforeSubmit();
    let extraInfo = JSON.parse(
      atob(searchQuery?.get("extra_info") || btoa("{}"))
    );
    if (!errorCheck) {
      setLoading(!loading);
      let splitDate = state?.expiry.split("/");
      let splitYear = moment(splitDate[1], "YY");
      let expiryYear = splitYear.format("YYYY");

      let data = {
        amount: paymentLink?.amount,
        amount_currency: paymentLink?.amount_currency,
        transaction_type: "Card",
        customer: {
          email: atob(searchQuery?.get("email")),
        },
        card: {
          card_number: state?.card.replace(/ /g, ""),
          card_type: formatCardType(cardType),
          exp_month: splitDate[0],
          exp_year: expiryYear,
          cvv: state?.cvv,
          country: "NGA",
        },
        source_identifier: paymentLink?.payment_type,
        source: "Payment Link",
        description: paymentLink?.description,
        meta: extraInfo,
        source_reference: atob(searchQuery?.get("code")),
        service: formatCardType(cardType),
      };

      try {
        let response = await createTransaction(
          paymentLink?.account,
          JSON.stringify(data)
        ).catch((error) => {
          setLoading(false);
          setFailed("Failed to send request");
          console.log(error);
        });

        let result = response;
        props.setCardList([...props.cardList, { ...state, cardType }]);
        //clear form after transaction (will be used ultimately)
        // dispatch({
        //   type: 'cleanState',
        //   data: cleanData,
        // });
        setCardType("");
        setLoading(false);

        //redirect to success page
        if (result?.data?.status) {
          history.push("/transaction/status");
        }

        //if an error is returned
        console.log(result?.data?.detail);
        setFailed(result?.data?.detail);
      } catch (error) {
        setLoading(false);
        setFailed("Failed to send request");
        console.log(error);
      }
    }
  };

  return (
    <section className={styles.body}>
      <div className="shadow px-5 pb-3 rounded-3">
        <div className={styles.logo_pay_info_container}>
          <img
            src={companyLogo}
            alt="Company Logo"
            className={styles.company_logo}
          />
          <div className={styles.pay_info_container}>
            <p>{email}</p>
            <p>
              Pay{" "}
              <span>{`${paymentLink.amount_currency} ${paymentLink?.amount}`}</span>
            </p>
          </div>
        </div>
        {failed !== null ? (
          <Alert color="danger" className={styles.enforce_red}>
            {failed}
          </Alert>
        ) : null}
        <form className="form d-block" id="paymentForm">
          <div className="my-3">
            <label className="form__form-group-label" htmlFor="card">
              Card Information
            </label>
            <div className={styles.clearfix}>
              <MaskedInput
                guide={false}
                mask={
                  ["37", "34"].includes(
                    state && state.card.split("").splice(0, 2).join("")
                  )
                    ? AMERICANEXPRESS
                    : OTHERCARDS
                }
                className="form-control"
                id="card"
                name="card"
                data-test="card"
                value={state.card}
                onChange={handleInputData}
                onBlur={handleBlur}
                placeholderChar={"\u2000"}
                required
                placeholder="5554-5678-3452-7845"
              />
              {(!error || !error.cardError) && CARDARR.includes(cardType) && (
                <img
                  style={{ float: "right", position: "relative", top: "-30px" }}
                  src={CARDICON[cardType]}
                  className={styles.cardType_img}
                  alt="Card"
                  width="50px"
                  height="30px"
                />
              )}
              {error && error.cardError && error.cardError.length > 1 && (
                <span className="form__form-group-error">
                  {error.cardError}
                </span>
              )}
              <div className={`${styles.card_date_cvv_container} input-group`}>
                <MaskedInput
                  mask={EXPIRYDATE}
                  guide={false}
                  required
                  name="expiry"
                  id="expiry"
                  data-test="expiryDate"
                  className={`${styles.card_date} form-control`}
                  value={state.expiry}
                  placeholder="MM/YY"
                  placeholderChar={"\u2000"}
                  onChange={handleInputData}
                  onBlur={handleBlur}
                />

                <MaskedInput
                  mask={CVV}
                  guide={false}
                  name="cvv"
                  min-length="3"
                  className={`${styles.card_cvv} form-control`}
                  id="cvv"
                  data-test="cvv"
                  required
                  value={state.cvv}
                  onChange={handleInputData}
                  onBlur={handleBlur}
                  placeholder="CVV"
                  placeholderChar={"\u2000"}
                />
              </div>
              {error && error.expiryError && error.expiryError.length > 1 && (
                <span className="form__form-group-error">
                  {error.expiryError}
                </span>
              )}
              {error &&
                error.securityCodeError &&
                error.securityCodeError.length > 1 && (
                  <span className="form__form-group-error">
                    {error.securityCodeError}
                  </span>
                )}
            </div>
          </div>
          <div className="my-3">
            <label className="form__form-group-label" htmlFor="name">
              Name on Card
            </label>
            <input
              onChange={handleInputData}
              onBlur={handleBlur}
              value={state.cardHolder}
              type="text"
              id="cardHolder"
              name="cardHolder"
              data-test="cardHolder"
              required
              placeholder=""
            />
            {error &&
              error.cardHolderError &&
              error.cardHolderError.length > 1 && (
                <span className="form__form-group-error">
                  {error.cardHolderError}
                </span>
              )}
          </div>

          <div className="my-3">
            <button
              type="submit"
              disabled={loading}
              data-test="cardSubmitButton"
              className="btn btn-primary btn-block "
              onClick={handleSubmit}
            >
              {!loading
                ? `Pay ${paymentLink.amount_currency} ${paymentLink?.amount}`
                : "Please Wait...."}
            </button>
            <div className="mt-3 text-center">
              <img
                className="align-middle d-inline-block"
                src={padlock}
                alt="Padlock"
                width="20px"
                height="20px"
              />{" "}
              <span className="align-middle ms-2">Secured By MiraPay</span>
            </div>
          </div>
        </form>
      </div>
    </section>
  );
};

// Export Component
export default PayForm;
