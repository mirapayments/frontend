//import "regenerator-runtime/runtime";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import Pay from "../index";
import { data } from "../../../fixtures/store";
import server from "../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname:
      "localhost:3000/pay?email=eWVraW5uaWppYm9sYUBnbWFpbC5jb20=&code=NTd5djM1b3RpZm82",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <Pay /> renders", () => {
  const store = mockStore(data);
  it("check inputs and button", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Pay />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(screen.getByText("NGN 100.00")).toBeInTheDocument();
      expect(screen.getByText("Card Information")).toBeInTheDocument();
      expect(screen.getByText("Name on Card")).toBeInTheDocument();
      expect(screen.getByText("Pay NGN 100.00")).toBeInTheDocument();
    });
  });
});
