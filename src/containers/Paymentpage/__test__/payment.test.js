//import "regenerator-runtime/runtime";
import { render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { BrowserRouter } from "react-router-dom";
import Paymentpage from "../index";
import { data } from "../../../fixtures/store";
import server from "../../../__mock__/server";

//mock redux store
const mockStore = configureStore([]);

//mock useHistory and useLocation
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "localhost:3000/pay?",
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("check if <Paymentpage /> renders", () => {
  const store = mockStore(data);
  it("check inputs and button", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Paymentpage />
        </BrowserRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(screen.getByText("School fees")).toBeInTheDocument();

      expect(screen.getByText("First Name")).toBeInTheDocument();
      expect(screen.getByText("Last Name")).toBeInTheDocument();
      expect(screen.getByText("Email")).toBeInTheDocument();
      expect(screen.getByText("Amount")).toBeInTheDocument();
      expect(screen.getByText("Pay")).toBeInTheDocument();
    });
  });
});
