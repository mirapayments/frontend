import styles from './payment.module.css';
import { useParams, useHistory } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
// import { removeEmpty } from "../../../../utils/removeEmptyObjectKeys/removeEmptyObjectKeys";
import { Formik, FieldArray } from 'formik';
import InfoCircleIcon from 'mdi-react/InfoCircleIcon';

import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';
import AlternateEmailIcon from 'mdi-react/AlternateEmailIcon';
import CurrencyUsdCircleIcon from 'mdi-react/CurrencyUsdCircleIcon';

//import PhoneIcon from 'mdi-react/PhoneClassicIcon';
import { useSelector } from 'react-redux';
import * as Yup from 'yup';
//import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
// import Swal from "sweetalert2";
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { getCurrencyTypes } from '../../../utils/apiHandlers/getCurrencyTypes';
//import PaymentModal from './ModalPayment/Modal';  
import { getPaymentLinks } from '../../../utils/apiHandlers/getPaymentLinks';
import { setPayLink, setPaymentLinkAmount } from '../../../redux/features/user/uiSlice';
import { useDispatch } from 'react-redux';
//import PaystackPop from './pay';
import padLock from '../../../shared/img/padlock.svg';
import { setLimitBasedOnCurrency } from "../../../utils/setLimitBasedOnCurrency/setLimitBasedOnCurrency";
//import { number } from 'prop-types';

const Paymentpage = ({ isActive, setIsActive }) => {
  // const { className } = useSelector((state) => state.theme);
  const { payid } = useParams();

  const dispatch = useDispatch();
  const history = useHistory();

  let [currency, setCurrency] = useState([]);
  let [isEditing, setIsEditing] = useState(false);
  
  let { paymentLink, currencies } = useSelector((state) => state.ui);
  const [modal, setModal] = useState(false);

  const toggleModal = () => {
    setModal((prev) => !prev);
  };

  const toggleEditing = () => {
    setIsEditing((prev) => !prev); 
  };

  function toCurrency(number) {
    const formatter = new Intl.NumberFormat("en-US", {style: 'decimal', currency: 'NGN'});
    return formatter.format(number);
  }

  const goToCardInfoInputStep = (values) => {
    let extraInfoObject = {};
    let count = 0;
    if(values?.extra_info?.length > 0){
      let valuesArray = values?.extra_info;

      values?.extra_info_fields?.forEach(value => {
        extraInfoObject = { ...extraInfoObject, [value]: valuesArray[count] };
        count++;
      });

    }

    if(!paymentLink?.fixed_amount){
      dispatch(setPaymentLinkAmount(values?.amount));
    }
    
    let check = values?.extra_info_fields.length > 0;
    let searchParamsString = `email=${btoa(values?.email)}${`&code=${btoa(values?.code)}`}${check? `&extra_info=${btoa(JSON.stringify(extraInfoObject))}` : '' }`;
    history.push(encodeURI(`/pay?${searchParamsString}`));
  }

  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'http://localhost:1337';
    script.async = true;
    document.body.appendChild(script);
  }, []);

  useEffect(() => {
    async function getCurrencyHandler() {
      try {
        const response = await getCurrencyTypes();
        const values = response.data.data.choices?.map((i) => {
          return {
            value: i,
            label: i,
          };
        });
        const currency = values || currencies;
        setCurrency(currency);
      } catch (error) {}
    }
    getCurrencyHandler();
  }, [currencies]);

  async function getPaymentLinksHandler() {
    try {
      const response = await getPaymentLinks(payid).catch((err) => {});
      if (response.data.status) {
        setIsActive(response.data.data.is_active);
        dispatch(setPayLink(response.data.data));
      } else {
        setIsActive(false);
        dispatch(setPayLink(null));
      }
    } catch (error) {

      console.log(error);
    }
  }
  useEffect(() => {
    getPaymentLinksHandler();
  }, [payid]);

  //setting up formik initial values.
  let initialValues = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '+234',
    currency: paymentLink?.amount_currency || 'NGN',
    amount: paymentLink?.amount || '',
    extra_info: [],
    extra_info_fields: paymentLink?.extra_info || [],
    code: paymentLink?.code
  };

  return isActive ===  undefined? null : (!paymentLink?.is_active === true ? (
    <p>
      Payment Link expired, or deleted. Please contact owner to create a new link and share with
      you.
    </p>
  ) : (
    <div className={`${styles.form_container} mt-4`} data-test="form-container">
      <div>
        <Formik
          initialValues={initialValues}
          onSubmit={(values, { setSubmitting }) => {
            console.log('ONSUBMIT', values);
            setSubmitting(true);
            //call paystack inline launch
            goToCardInfoInputStep(values);
            toggleModal();
          }}
          validationSchema={Yup.object().shape({
            first_name: Yup.string()
              .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed as first name')
              .required('Please provide your first name.')
              .min(4, 'First name should be a minimum of 3 characters.'),
            last_name: Yup.string()
              .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed as last name.')
              .required('Please provide your last name.')
              .min(4, 'Last name should be a minimum of 3 characters.'),
            phone: Yup.string().notRequired(),
            email: Yup.string().email('Please provide a valid email address.').required(),
            currency: Yup.string().notRequired(),
            amount: Yup.number()
              .min(setLimitBasedOnCurrency(paymentLink?.amount_currency),
              `Amount should be greater than or equal to ${setLimitBasedOnCurrency(paymentLink?.amount_currency)}`)
              .required('Please enter the required amount'),
            extra_info: Yup.array().notRequired(),
          })}
        >
          {(props) => {
            const { values, touched, errors, handleChange, handleBlur, handleSubmit } = props;
            return (
              <form className="form" onSubmit={handleSubmit}>
                <div className={styles.name_container}>
                  <div className={`form__form-group`}>
                    <span className="form__form-group-label">First Name</span>
                    <div
                      className={`form__form-group-field ${
                        errors.first_name && touched.first_name && 'form__form-validation'
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <AccountOutlineIcon />
                      </div>
                      <input
                        name="first_name"
                        component="input"
                        type="text"
                        placeholder="First Name"
                        data-test="first-name-input"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.first_name}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.first_name && touched.first_name && errors.first_name}
                    </p>
                  </div>
                  <div className="form__form-group">
                    <span className="form__form-group-label">Last Name</span>
                    <div
                      className={`form__form-group-field ${
                        errors.last_name && touched.last_name && 'form__form-validation'
                      }`}
                    >
                      <div className="form__form-group-icon">
                        <AccountOutlineIcon />
                      </div>
                      <input
                        name="last_name"
                        component="input"
                        type="text"
                        placeholder="Last Name"
                        data-test="last-name-input"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.last_name}
                      />
                    </div>
                    <p className="error_validation_text">
                      {errors.last_name && touched.last_name && errors.last_name}
                    </p>
                  </div>
                </div>
                <div className="form__form-group">
                  <span className="form__form-group-label">Email</span>
                  <div
                    className={`form__form-group-field ${errors.email && 'form__form-validation'}`}
                  >
                    <div className="form__form-group-icon">
                      <AlternateEmailIcon />
                    </div>
                    <input
                      name="email"
                      component="input"
                      type="email"
                      placeholder="Email"
                      data-test="email-input"
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                  <p className="error_validation_text">
                    {errors.email && touched.email && errors.email}
                  </p>
                </div>
                {/* <div>
                  <span className="form__form-group-label">Phone Number</span>
                  <div
                    className={`form__form-group-field`}
                  >
                    <div className="form__form-group-icon">
                      <PhoneIcon />
                    </div>
                    <PhoneInput
                      inputProps={{ name: "phone" }}
                      onChange={(phone, country, e) => {
                        handleChange(e);
                      }}
                      placeholder="Enter phone number"
                      name="phone"
                      value={values.phone}
                      containerStyle={{ width: "372px" }}
                    />
                  </div>
                  <p className="error_validation_text">
                    {errors.phone && touched.phone && errors.phone}
                  </p>
                </div> */}
                <div>
                  <span className={`form__form-group-label`}>Amount</span>
                  <div className={styles.currency_select_container}>
                    <div className={`${styles.currency_select} form__form-group-field`}>
                      <div className="form__form-group-icon">
                        <CurrencyUsdCircleIcon />
                      </div>
                      <TextField
                        
                        disabled={paymentLink?.fixed_amount}
                        select
                        InputLabelProps={{ shrink: true }}
                        margin="normal"
                        name="currency"
                        data-test="currency-input"
                        value={paymentLink?.amount_currency}
                        onChange={handleChange}
                      >
                        {currency.map((option) => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </div>
                    <div
                      className={`form__form-group-field ${
                        errors.email && 'form__form-validation'
                      }`}
                    >
                      {
                        !paymentLink?.fixed_amount ? (<input
                          style={{
                            width: '330px',
                          }}
                          disabled={paymentLink?.fixed_amount}
                          name="amount"
                          component="input"
                          type="number"
                          placeholder="Amount"
                          onChange={handleChange}
                          data-test="amount-input"
                          onBlur={toggleEditing}
                          
                        />) : (<input
                          style={{
                            width: '330px',
                          }}
                          disabled={paymentLink?.fixed_amount}
                          name="amount"
                          component="input"
                          type="text"
                          placeholder="Amount"
                          data-test="amount-input"
                          onFocus={toggleEditing}
                          value={toCurrency(paymentLink?.amount)}
                        />) 
                      }
                      {console.log(paymentLink?.fixed_amount)}
                    </div>
                  </div>
                  <p className="error_validation_text">
                    {errors.amount && touched.amount && errors.amount}
                  </p>

                  {/* render extra info if there are extra infos */}
                  {paymentLink?.extra_info?.length > 0 && (
                    <div className={`form__form-group`}>
                      <span className="form__form-group-label mt-4">Extra Information</span>
                      <div className={`form__form-group-field mt-4 `}>
                        <FieldArray
                          className={styles.extra_info}
                          name="extra_info"
                          render={(arrayHelpers) => (
                            <div>
                              {paymentLink?.extra_info?.length > 0 &&
                                paymentLink?.extra_info.map((extra_info, index) => (
                                  <div key={index} className="form__form-group ">
                                    <span className="form__form-group-label">
                                      {/* Check if payment link is not null before rendering */}
                                      {paymentLink === null
                                        ? paymentLink
                                        : paymentLink?.extra_info[index]}
                                    </span>
                                    <div
                                      className={`form__form-group-field ${
                                        errors.email && 'form__form-validation'
                                      }`}
                                    >
                                      <div
                                        className={`form__form-group-icon ${styles.select_form_custom_icon}`}
                                      >
                                        <InfoCircleIcon />
                                      </div>
                                      <input
                                        style={{ width: '370px' }}
                                        name={`extra_info.${index}`}
                                        component="input"
                                        type="text"
                                        placeholder={`${paymentLink?.extra_info[index]}`}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                      />
                                    </div>
                                  </div>
                                ))}
                            </div>
                          )}
                        />
                      </div>
                    </div>
                  )}
                </div>
                <div className={styles.btnContainer}>
                  {/* <PaymentModal toggle={toggleModal} modalOpen={modal} /> */}
                  <button
                    type="submit"
                    data-test="pay-button"
                    className={`${styles.button_newaccount} btn btn-primary account__btn account__btn--small`}
                  >
                    Pay
                  </button>
                </div>
              </form>
            );
          }}
        </Formik>
      </div>
      <div className="footer">
        <img className="align-top" src={padLock} alt="Padlock" width="20px" />{' '}
        <span className="align-middle ml-2">Secured By MiraPay</span>
      </div>
    </div>
  ))
};

export default Paymentpage;
