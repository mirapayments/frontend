import React, { useState } from "react";
import Payment from "./components/Paymentpage";
import styles from "./payment.module.css";
import MirapayLogo from "../../shared/img/mirapaylogo.png";
import { useSelector } from "react-redux";

const Paymentpage = () => {
  const [isActive, setIsActive] = useState(undefined);
  const { paymentLink } = useSelector((state) => state.ui);

  return (
    <>
      <div className="account">
        <div className={`${styles.wrapper} account__wrapper`}>
          <div className={`${styles.container} account__card`}>
            <div className={`${styles.wrapper} account__wrapper`}>
              <img alt="Company logo" className={styles.logo} src={MirapayLogo} />
              { isActive === undefined ? null : (paymentLink?.is_active ? (
                <>
                  <h4 className={styles.description_text}>{paymentLink?.name}</h4>
                  <h5
                    className={`${styles.description_text} ${styles.muted_text} `}
                  >
                    BY {paymentLink?.account_detail?.account_name?.toUpperCase()}
                  </h5>
                  <h5 className={`${styles.description_text} mb-4 mt-4`}>
                    {paymentLink?.description}
                  </h5>
                </>
              ) : (
                ""
              ))}
            </div>
            <Payment isActive={isActive} setIsActive={setIsActive} />
          </div>
        </div>
      </div>
    </>
  );
};

export default Paymentpage;
