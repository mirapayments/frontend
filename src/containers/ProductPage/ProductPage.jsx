// Import the necessary libraries and modules.
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import '../Home/Pages.css';
import hero from './webp/heroPayment.webp';
import guard from '../Home/gifs/security-pay.gif';

// Product Page Component.
const ProductPage = () => {
  return (
    <>
      <main>
        <div className="payment-hero-wrapper">
          <div className="nav-wrapper">
            <nav className="navbar navbar-expand-lg align-items-center navbar-light">
              <div className="container-fluid">
                <Link to="/" className="logo">
                  <p className="logo-text">MiraPay</p>
                </Link>
                {/* Hamburger button */}
                <button
                  type="button"
                  className="navbar-toggler"
                  data-bs-target="#offcanvasNavbar"
                  data-bs-toggle="offcanvas"
                >
                  <span className="navbar-toggler-icon" />
                </button>

                <div
                  className="offcanvas offcanvas-end"
                  tabIndex="-1"
                  id="offcanvasNavbar"
                  aria-labelledby="offcanvasNavbarLabel"
                >
                  <div className="offcanvas-header">
                    <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                      MiraPay
                    </h5>
                    <button
                      type="button"
                      className="btn-close text-reset"
                      data-bs-dismiss="offcanvas"
                      aria-label="Close"
                    />
                  </div>

                  <div className="offcanvas-body align-items-center justify-content-between">
                    <ul className="navbar-nav mx-auto nav_list" id="navbar">
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/" exact className="nav-link mx-md-2">
                          Home
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                          About Us
                        </NavLink>
                      </li>
                      <li className="nav-item dropdown">
                        <Link
                          to="#"
                          className="nav-link dropdown-toggle mx-md-2"
                          role="button"
                          data-bs-toggle="dropdown"
                        >
                          Products
                        </Link>
                        <ul className="dropdown-menu">
                          <li>
                            <Link className="dropdown-item" to="/payment">
                              Payment Links
                            </Link>
                          </li>
                        </ul>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/testimonial"
                          className="nav-link mx-md-2"
                        >
                          Testimonials
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/pricing"
                          className="nav-link mx-md-2"
                        >
                          Pricing
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/contact"
                          className="nav-link mx-md-2"
                        >
                          Contact Us
                        </NavLink>
                      </li>
                    </ul>

                    <div className="my-auto">
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                          Sign In
                        </button>
                      </a>
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-1 btn-primary my-0 py-md-2">
                          Sign Up For Free
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>

          <section className="hero payment-hero py-3">
            <div className="payment pt-4 mt-3">
              <div className="container-fluid mt-md-5">
                <div className="row">
                  <div className="col-md-6 my-md-auto">
                    <h1 className="payment-header">
                      Payments made easy for everyone and every business
                    </h1>
                    <p className="payment-text my-4">
                      <span className="font-weight-bold">MiraPay</span> allows different variety of
                      people (Individuals, NGOs, Government, Religious Bodies etc) to receive
                      payments and also make payments accessible from every part of the world for
                      everyone.
                    </p>
                    <button className="btn btn-primary">Sign Up for Free</button>
                    <button className="btn btn-light btn-contact">Contact Sales</button>
                  </div>
                  <div className="col-md-6 my-md-auto">
                    <img loading="lazy" className="img-fluid" src={hero} alt="Hero" />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <section className="more my-5 py-md-5">
          <h2 className="more-header text-center mb-3">Do More With MiraPay</h2>
          <div className="container-fluid">
            <div className="row g-5 pt-md-3 mt-md-3">
              <div className="col-md-6">
                <img loading="lazy" className="img-fluid" src={hero} alt="Hero" />
              </div>
              <div className="col-md-6 m-auto ps-md-3">
                <h3 className="work-process-header my-3">How it actually works</h3>
                <div className="process-list">
                  <div className="process-section d-flex justify-content-between">
                    <div className="individual-process">
                      <div className="process-header mt-md-3 mb-md-2">
                        <span className="number">01</span>
                        <span className="process-title">Single Payment</span>
                      </div>

                      <div className="process d-flex">
                        <span className="line" />
                        <span className="process-text">
                          Create a payment link to receive a one-time payment from your customer,
                          modify the currency as needed.
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="services-section d-flex justify-content-between">
                    <div className="individual-process">
                      <div className="process-header mt-md-3 mb-md-2">
                        <span className="number">02</span>
                        <span className="process-title">Subscription</span>
                      </div>

                      <div className="process d-flex">
                        <span className="line" />
                        <span className="process-text">
                          Create a payment link to receive a recurring payment from your customer,
                          select the interval and currency
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="services-section d-flex justify-content-between">
                    <div className="individual-process">
                      <div className="process-header mt-md-3 mb-md-2">
                        <span className="number">03</span>
                        <span className="process-title">Support</span>
                      </div>

                      <div className="process d-flex">
                        <span className="line" />
                        <span className="process-text">
                          Create a payment link to receive payments from your well wishers or
                          supporters
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="services-section d-flex justify-content-between">
                    <div className="individual-process">
                      <div className="process-header mt-md-3 mb-md-2">
                        <span className="number">04</span>
                        <span className="process-title">Get Paid</span>
                      </div>

                      <div className="process d-flex">
                        <span className="process-text-last">
                          Share the generated payment link to your customers, users, supporters or
                          well wishes to pay you securely,
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="integration my-md-5 py-md-5">
          <h2 className="integration-header my-3 text-center">No Integration Needed</h2>
          <div className="container-fluid">
            <div className="row g-0 mt-md-5">
              <div className="col-md-6 my-lg-auto">
                <h4 className="integration-title pb-md-3">
                  No need for any form of integration to get through with the payment link
                </h4>
                <p className="integration-text my-3">
                  With payment links, you don’t need any integrations. All you should do is share
                  your link, take a seat while you sip your drink and receive your alerts! How sweet
                  😊
                </p>
              </div>

              <div className="col-md-6">
                <img loading="lazy" className="img-fluid" src={hero} alt="No Integration" />
              </div>
            </div>
          </div>
        </section>

        <footer className="footer my-md-5 py-md-5">
          <section className="security-section">
            <div className="row">
              <div className="col-md-6">
                <h2 className="security-header">Guaranteed Security and Safety</h2>
                <div className="my-3">
                  <p className="security-title">Your business is properly secured</p>
                  <p className="security-text">
                    We have put infrastructures in place to help your stay secure in the midst of
                    different level of fraudulent activity all over the internet
                  </p>
                </div>

                <div className="my-md-3">
                  <p className="security-title">
                    You and your customers are protected with advanced fraud detection
                  </p>
                  <p className="security-text">
                    All payments and transactions are secured end to end with our high security
                    infrastructure. We have no tolerance for fraud
                  </p>
                </div>
              </div>

              <div className="col-md-6 guide-image">
                <img loading="lazy" className="img-fluid" src={guard} alt="guard" />
              </div>
            </div>
          </section>

          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">Start accepting payments now!</h2>

                    <p className="question-text mt-0 py-3">
                      Do you want to get paid now? Do you want to start accepting payments in less
                      than 30 minutes? You are in the right place. Get started now!
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <a className="sitemap-contact m-0" href="mailto:mirapaymentsltd@gmail.com">
                    mirapaymentsltd@gmail.com
                  </a>
                  <p>
                    <a className="sitemap-contact" href="tel:+2347069179050">
                      +234 7069179050
                    </a>
                  </p>
                  <p className="sitemap-contact">Lagos, Nigeria</p>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quick Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/testimonial">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/payment">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Terms & Condition</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

// Export Component.
export default ProductPage;
