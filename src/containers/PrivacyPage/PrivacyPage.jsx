// Import all necessary libraries and modules.
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import '../Home/Pages.css';

// Privacy page Component.
const PrivacyPage = () => {
  return (
    <>
      <main>
        <div className="nav-wrapper">
          <nav className="navbar navbar-expand-lg align-items-center navbar-light">
            <div className="container-fluid">
              <Link to="/" className="logo">
                <p className="logo-text">MiraPay</p>
              </Link>
              {/* Hamburger button */}
              <button
                type="button"
                className="navbar-toggler"
                data-bs-target="#offcanvasNavbar"
                data-bs-toggle="offcanvas"
              >
                <span className="navbar-toggler-icon" />
              </button>

              <div
                className="offcanvas offcanvas-end"
                tabIndex="-1"
                id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel"
              >
                <div className="offcanvas-header">
                  <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                    MiraPay
                  </h5>
                  <button
                    type="button"
                    className="btn-close text-reset"
                    data-bs-dismiss="offcanvas"
                    aria-label="Close"
                  />
                </div>

                <div className="offcanvas-body align-items-center justify-content-between">
                  <ul className="navbar-nav mx-auto nav_list" id="navbar">
                    <li className="nav-item">
                      <NavLink exact activeClassName="active" to="/"  className="nav-link mx-md-2">
                        Home
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                        About Us
                      </NavLink>
                    </li>
                    <li className="nav-item dropdown">
                      <Link
                        to="#"
                        className="nav-link dropdown-toggle mx-md-2"
                        role="button"
                        data-bs-toggle="dropdown"
                      >
                        Products
                      </Link>
                      <ul className="dropdown-menu">
                        <li>
                          <Link className="dropdown-item" to="/payment">
                            Payment Links
                          </Link>
                        </li>
                        <li>
                          <Link className="dropdown-item" to="#">
                            Another link
                          </Link>
                        </li>
                        <li>
                          <Link className="dropdown-item" to="#">
                            A third link
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item">
                      <NavLink
                        activeClassName="active"
                        to="/testimonial"
                        className="nav-link mx-md-2"
                      >
                        Testimonials
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink activeClassName="active" to="/pricing" className="nav-link mx-md-2">
                        Pricing
                      </NavLink>
                    </li>
                    <li className="nav-item">
                      <NavLink activeClassName="active" to="/contact" className="nav-link mx-md-2">
                        Contact Us
                      </NavLink>
                    </li>
                  </ul>

                  <div className="my-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                        Sign In
                      </button>
                    </a>
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn border-1 btn-primary my-0 py-md-2">
                        Sign Up For Free
                      </button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>

        <section className="privacy-policy pt-md-5 mt-md-5">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-3">
                <div id="list-example" className="list-group">
                  <a className="list-group-item list-group-item-action" href="#list-item-1">
                    Item 1
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-2">
                    Item2
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-3">
                    Item 3
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-4">
                    Item 4
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-5">
                    Item 5
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-6">
                    Item 6
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-7">
                    Item 7
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-8">
                    Item 8
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-9">
                    Item 9
                  </a>
                  <a className="list-group-item list-group-item-action" href="#list-item-10">
                    Item 10
                  </a>
                </div>
              </div>

              <div className="col-md-9">
                <div
                  data-bs-spy="scroll"
                  data-bs-target="#list-example"
                  data-bs-offset="10"
                  tabIndex="0"
                  className="scrollspy-example"
                >
                  <h4 id="list-item-1">Item 1</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do amet
                    fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum
                    fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do amet
                    fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum
                    fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.
                  </p>
                  <h4 id="list-item-2">Item 2</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do amet
                    fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum
                    fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do amet
                    fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum
                    fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.
                  </p>
                  <h4 id="list-item-3">Item 3</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do amet
                    fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum
                    fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do amet
                    fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum
                    fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.
                  </p>
                  <h4 id="list-item-4">Item 4</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.
                  </p>
                  <h4 id="list-item-5">Item 5</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea ea do
                    reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident occaecat
                    excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor mollit elit
                    ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex ullamco
                    excepteur.
                  </p>
                  <h4 id="list-item-6">Item 6</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.
                  </p>
                  <h4 id="list-item-7">Item 7</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea ea do
                    reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident occaecat
                    excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor mollit elit
                    ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex ullamco
                    excepteur.
                  </p>
                  <h4 id="list-item-8">Item 8</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea ea do
                    reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident occaecat
                    excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor mollit elit
                    ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex ullamco
                    excepteur.
                  </p>
                  <h4 id="list-item-9">Item 9</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea ea do
                    reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident occaecat
                    excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor mollit elit
                    ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex ullamco
                    excepteur.
                  </p>
                  <h4 id="list-item-10">Item 10</h4>
                  <p>
                    Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit culpa duis.
                    Nostrud aliqua ipsum fugiat minim proident occaecat excepteur aliquip culpa aute
                    tempor reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit
                    fugiat mollit culpa irure ullamco est ex ullamco excepteur. Quis anim sit do
                    amet fugiat dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua
                    ipsum fugiat minim proident occaecat excepteur aliquip culpa aute tempor
                    reprehenderit. Deserunt tempor mollit elit ex pariatur dolore velit fugiat
                    mollit culpa irure ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat
                    dolor velit sit ea ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat
                    minim proident occaecat excepteur aliquip culpa aute tempor reprehenderit.
                    Deserunt tempor mollit elit ex pariatur dolore velit fugiat mollit culpa irure
                    ullamco est ex ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea
                    ea do reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident
                    occaecat excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor
                    mollit elit ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex
                    ullamco excepteur.Quis anim sit do amet fugiat dolor velit sit ea ea do
                    reprehenderit culpa duis. Nostrud aliqua ipsum fugiat minim proident occaecat
                    excepteur aliquip culpa aute tempor reprehenderit. Deserunt tempor mollit elit
                    ex pariatur dolore velit fugiat mollit culpa irure ullamco est ex ullamco
                    excepteur.Quis anim sit do amet fugiat dolor velit sit ea ea do reprehenderit
                    culpa duis. Nostrud aliqua ipsum fugiat minim proident occaecat excepteur
                    aliquip culpa aute tempor reprehenderit. Deserunt tempor mollit elit ex pariatur
                    dolore velit fugiat mollit culpa irure ullamco est ex ullamco excepteur.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <footer className="footer my-5 py-md-5">
          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">
                      Want to start accepting payments in less 30mins?
                    </h2>

                    <p className="question-text mt-0 py-3">
                      Lorem ipsum dolor sit amet,Ut varius cras feugiat feugiat Lorem ipsum dolor
                      sit amet,Ut varius cras feugiat feugiat Lorem ipsum dolor sit amet,Ut varius
                      cras feugiat feugiat Lorem ipsum dolor sit amet,Ut varius cras feugiat feugiat
                      Lorem ipsum dolor sit amet,Ut varius cras feugiat feugiat Lorem ipsum dolor
                      sit amet,Ut varius cras feugiat feugiat
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <p className="sitemap-contact m-0">johndoe@mirapay.com</p>
                  <p className="sitemap-contact">+233 01245678910</p>
                  <p className="sitemap-contact">2, Lagos street,</p>
                  <span className="lagos">Lagos</span>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quicks Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-terms m-0">Terms & Condition</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

// Export Component
export default PrivacyPage;
