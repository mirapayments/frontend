import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import './Pages.css';
import hero from './webp/hero_image.webp';
import seamless from './webp/seamless_image.webp';
import checker from './svgs/checker.svg';
import phone from './gifs/mobile-payment.gif';
import guard from './gifs/security-pay.gif';
import service from './webp/services.webp';;

// Homepage Component.
const Homepage = () => {
  return (
    <>
      <main className="container-fluid-md">
        <div className="hero-wrapper py-3">
          <div className="nav-wrapper">
            <nav className="navbar navbar-expand-lg align-items-center navbar-light">
              <div className="container-fluid">
                <Link to="/" className="logo">
                  <p className="logo-text">MiraPay</p>
                </Link>
                {/* Hamburger button */}
                <button
                  type="button"
                  className="navbar-toggler"
                  data-bs-target="#offcanvasNavbar"
                  data-bs-toggle="offcanvas"
                >
                  <span className="navbar-toggler-icon" />
                </button>

                <div
                  className="offcanvas offcanvas-end"
                  tabIndex="-1"
                  id="offcanvasNavbar"
                  aria-labelledby="offcanvasNavbarLabel"
                >
                  <div className="offcanvas-header">
                    <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                      MiraPay
                    </h5>
                    <button
                      type="button"
                      className="btn-close text-reset"
                      data-bs-dismiss="offcanvas"
                      aria-label="Close"
                    />
                  </div>

                  <div className="offcanvas-body align-items-center justify-content-between">
                    <ul className="navbar-nav mx-auto nav_list" id="navbar">
                      <li className="nav-item">
                        <NavLink exact activeClassName="active" to="/" className="nav-link mx-md-2">
                          Home
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink activeClassName="active" to="/about" className="nav-link mx-md-2">
                          About Us
                        </NavLink>
                      </li>
                      <li className="nav-item dropdown">
                        <Link
                          to="#"
                          className="nav-link dropdown-toggle mx-md-2"
                          role="button"
                          data-bs-toggle="dropdown"
                        >
                          Products
                        </Link>
                        <ul className="dropdown-menu">
                          <li>
                            <Link className="dropdown-item" to="/payment">
                              Payment Links
                            </Link>
                          </li>
                        </ul>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/testimonial"
                          className="nav-link mx-md-2"
                        >
                          Testimonials
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/pricing"
                          className="nav-link mx-md-2"
                        >
                          Pricing
                        </NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink
                          activeClassName="active"
                          to="/contact"
                          className="nav-link mx-md-2"
                        >
                          Contact Us
                        </NavLink>
                      </li>
                    </ul>

                    <div className="my-auto">
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-0 btn-outline-primary text-black my-0 me-2 p-md-2">
                          Sign In
                        </button>
                      </a>
                      <a
                        href="https://tinyurl.com/mirapaywaitinglink"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button className="btn border-1 btn-primary my-0 py-md-2">
                          Sign Up For Free
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>

          <section className="hero">
            <div className="onlinepayment pt-3 mt-3">
              <div className="container-fluid my-3">
                <div className="row">
                  <div className="col-md-6 my-md-auto">
                    <header>
                      <h1 className="onlinepayment-header">
                        Easy online payment gateway for everyone
                      </h1>
                      <p className="onlinepayment-text my-4">
                        <span className="font-weight-bold">MiraPay</span> allows different variety
                        of people (Individuals, NGOs, Government, Religious Bodies etc) to receive
                        payments and also make payments accessible from every part of the world for
                        everyone.
                      </p>
                      <button className="btn btn-primary">Sign Up for Free</button>
                      <button className="btn btn-light btn-contact">Contact Sales</button>
                    </header>
                  </div>
                  <div className="col-md-6 my-md-auto">
                    <img loading="lazy" className="img-fluid" src={hero} alt="Hero" />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <section className="seamless-payment my-md-5 py-md-5">
          <h1 className="my-5 mx-auto text-center seamless-header">
            Maintain a Seamless Payment Experience
          </h1>
          <div className="row g-0 mt-md-5">
            <div className="col-md-7">
              <img loading="lazy" className="img-fluid" src={seamless} alt="Seamless" />
            </div>

            <div className="col-md-4 my-lg-auto purpose px-4 mx-xs-4 ">
              <h3 className="purpose-header my-3">
                An excellent platform to help your business with different kinds of payments as it
                serves the needs of the users.
              </h3>
              <p className="purpose-text my-3">
                MiraPay lowers the barrier of receiving payments from anywhere in the world. Some of
                the advantages of this payment gateway is to reduce cost in your various
                transactions, availability to a wider variety of users, high transaction success
                rates and easy integration
              </p>

              <button className="btn mt-3 mb-0 btn-primary">Contact Sales</button>
            </div>
          </div>
        </section>

        {/* <section className="services my-md-5 py-md-5">
          <h2 className="service-header text-center my-md-3 py-md-3">
            Our Services
          </h2>
          <p className="our-service">
            We offer new generation online payments to help you scale your
            business in a very short time. We support a wide variety of payment
            channels, so you wouldn't have to worry about not receiving payments
            from a set group of individuals.
          </p>
          <div className="container-fluid mx-auto">
            <div className="row my-3 py-3">
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={card} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Card</h5>
                  <p className="service-text my-3">
                    We process card payments in a fast and secure way. Card
                    payments have never been this seamless
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={bankCard} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Bank Account</h5>
                  <p className="service-text my-3">
                    Choose your bank among the supported banks and begin
                    payments
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                <img className="mb-2" src={smarttouch} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Bank Transfer</h5>
                  <p className="service-text my-3">
                    Users can pay using our automatically generated bank
                    accounts
                  </p>
                </div>
              </div>
            </div>

            <div className="row mb-3 pb-3">
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={balance} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">USSD</h5>
                  <p className="service-text my-3">
                    Users can make payments from any mobile device with our USSD
                    payment platform
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                <img className="mb-2" src={bankTerminal} alt="Card" width="60px" height="60px" />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">POS(coming soon)</h5>
                  <p className="service-text my-3">
                    In a short time, users can make payments using point of sale
                    devices
                  </p>
                </div>
              </div>
              <div className="col-md-4 my-3 service">
                <div>
                  <img className="mb-2" src={code} alt="QR Code" width="60px" height="60px"  />
                </div>
                <div className="individual-service p-0 ">
                  <h5 className="service-title">Visa QR (coming soon)</h5>
                  <p className="service-text my-3">
                    In a short while, you’ll just have to scan a code to make
                    payments
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section> */}

        <section className="services my-md-5 py-md-5">
          <div className="container-fluid mt-3">
            <div className="row g-5">
              <div className="col-md-6">
                <h2 className="service-header my-3 pb-md-5">Our Services</h2>
                <p className="our-service-home">
                  We offer new generation online payments to help you scale your business in a very
                  short time. We support a wide variety of payment channels, so you wouldn`&apos;`t have to
                  worry about not receiving payments from a set group of individuals.
                </p>

                <div className="w-100">
                  <div className="row">
                    <div className="col services-section">
                      <div className="individual-service w-100">
                        <div className="service-header mt-md-3 mb-md-2">
                          <span className="number">01</span>
                          <span className="service-title-home">Card</span>
                        </div>

                        <div className="service d-flex">
                          <span className="line" />
                          <span className="service-text-home">
                            We process card payments in a fast and secure way. Card payments have
                            never been this seamless
                          </span>
                        </div>
                      </div>
                    </div>

                    <div className="col services-section">
                      <div className="individual-service w-100">
                        <div className="service-header mt-md-3 mb-md-2">
                          <span className="number">04</span>
                          <span className="service-title-home">USSD</span>
                        </div>

                        <div className="service d-flex">
                          <span className="line" />
                          <span className="service-text-home">
                            Users can make payments from any mobile device with our USSD payment
                            platform
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col services-section">
                      <div className="individual-service w-100">
                        <div className="service-header mt-md-3 mb-md-2">
                          <span className="number">02</span>
                          <span className="service-title-home">Bank Account</span>
                        </div>

                        <div className="service d-flex">
                          <span className="line" />
                          <span className="service-text-home">
                            Choose your bank among the supported banks and begin payments
                          </span>
                        </div>
                      </div>
                    </div>

                    <div className="col services-section">
                      <div className="individual-service w-100">
                        <div className="service-header mt-md-3 mb-md-2">
                          <span className="number">05</span>
                          <span className="service-title-home">POS(coming soon)</span>
                        </div>

                        <div className="service d-flex">
                          <span className="line" />
                          <span className="service-text-home">
                            In a short time, users can make payments using point of sale devices
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col services-section">
                      <div className="individual-service">
                        <div className="service-header mt-md-3 mb-md-2">
                          <span className="number">03</span>
                          <span className="service-title-home">Bank Transfer</span>
                        </div>

                        <div className="service d-flex">
                          <span className="service-text-last">
                            Users can pay using our automatically generated bank accounts
                          </span>
                        </div>
                      </div>
                    </div>

                    <div className="col services-section">
                      <div className="individual-service">
                        <div className="service-header mt-md-3 mb-md-2">
                          <span className="number">06</span>
                          <span className="service-title-home">Visa QR (coming soon)</span>
                        </div>

                        <div className="service d-flex">
                          <span className="service-text-last">
                            In a short while, you’ll just have to scan a code to make payments
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* <div className="services-list d-flex justify-content-between my-md-3">
                  </div>

                  <div className="services-list d-flex justify-content-between my-md-3">
                  </div>

                  <div className="services-list d-flex justify-content-between my-md-3">
                  </div> */}
                </div>
              </div>

              <div className="col-md-6 ps-sm-5 service-image">
                <img loading="lazy" className="img-fluid" src={service} alt="" />
              </div>
            </div>
          </div>
        </section>

        <section className="api-section mt-md-5 pt-md-5">
          <div className="row row-api-section">
            <div className="col-md-6 api-image">
              <img loading="lazy" className="phone img-fluid" src={phone} alt="Api Phone" />
            </div>

            <div className="col-md-6">
              <h2 className="api-header mt-5 mb-3">
                Our well documented and easy-to-use APIs help our users build amazing products in
                minutes
              </h2>

              <p className="api-text my-md-5">
                Because we have developers in mind, we take the heavy lift, giving you a simple
                interface so your teams don’t need to stitch together disparate systems or spend
                months integrating payments functionality.
              </p>

              <div className="d-sm-flex my-4 mx-auto api-lists-section">
                <ul className="api-lists p-md-0">
                  <li className="py-2 list">
                    <img
                      loading="lazy"
                      className="img-fluid checker pe-2"
                      src={checker}
                      alt="checker"
                    />
                    We follow REST principles
                  </li>
                  <li className="py-2 list">
                    <img
                      loading="lazy"
                      className="img-fluid checker pe-2"
                      src={checker}
                      alt="checker"
                    />
                    We take security seriously
                  </li>
                  <li className="py-2 list">
                    <img
                      loading="lazy"
                      className="img-fluid checker pe-2"
                      src={checker}
                      alt="checker"
                    />
                    Error messages are useful
                  </li>
                </ul>

                <ul className="api-lists">
                  <li className="py-2 list">
                    <img
                      loading="lazy"
                      className="img-fluid checker pe-2"
                      src={checker}
                      alt="checker"
                    />
                    We support filtering
                  </li>
                  <li className="py-2 list">
                    <img
                      loading="lazy"
                      className="img-fluid checker pe-2"
                      src={checker}
                      alt="checker"
                    />
                    We provide simplicity
                  </li>
                  <li className="py-2 list">
                    <img
                      loading="lazy"
                      className="img-fluid checker pe-2"
                      src={checker}
                      alt="checker"
                    />
                    Our API is consistent and symmetrical
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>

        <footer className="footer my-md-5 py-md-5">
          <section className="security-section">
            <div className="row">
              <div className="col-md-6">
                <h2 className="security-header">Guaranteed Security and Safety</h2>
                <div className="my-3">
                  <p className="security-title">Your business is properly secured</p>
                  <p className="security-text">
                    We have put infrastructures in place to help your stay secure in the midst of
                    different level of fraudulent activity all over the internet
                  </p>
                </div>

                <div className="my-md-3">
                  <p className="security-title">
                    You and your customers are protected with advanced fraud detection
                  </p>
                  <p className="security-text">
                    All payments and transactions are secured end to end with our high security
                    infrastructure. We have no tolerance for fraud
                  </p>
                </div>
              </div>

              <div className="col-md-6 guide-image">
                <img loading="lazy" className="img-fluid" src={guard} alt="guard" />
              </div>
            </div>
          </section>

          <div className="question-wrapper my-md-5">
            <section className="question-section my-3">
              <div className="container-fluid">
                <div className="row my-md-auto">
                  <div className="col-md-8 question-text-section">
                    <h2 className="question-header mt-3">Start accepting payments now!</h2>

                    <p className="question-text mt-0 py-3">
                      Do you want to get paid now? Do you want to start accepting payments in less
                      than 30 minutes? You are in the right place. Get started now!
                    </p>
                  </div>

                  <div className="col-md-4 my-md-auto">
                    <a
                      href="https://tinyurl.com/mirapaywaitinglink"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <button className="btn btn-primary">Sign Up For Free</button>
                    </a>
                    <button className="btn btn-light ms-3 btn-contact">Contact Sales</button>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <section className="footer-section my-md-5 py-md-5">
            <div className="container-fluid mt-3">
              <div className="row">
                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">MiraPay</p>
                  <a className="sitemap-contact m-0" href="mailto:mirapaymentsltd@gmail.com">
                    mirapaymentsltd@gmail.com
                  </a>
                  <p>
                    <a className="sitemap-contact" href="tel:+2347069179050">
                      +234 7069179050
                    </a>
                  </p>
                  <p className="sitemap-contact">Lagos, Nigeria</p>
                </div>

                <div className="col-md-3 sitemap">
                  <p className="sitemap-head">Quick Links</p>
                  <p className="sitemap-link m-0">
                    <Link to="/">Home</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/about">About Us</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/testimonial">Testimonials</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/payment">Payment Links</Link>
                  </p>
                </div>

                <div className="col-md-2 sitemap">
                  <p className="sitemap-head">Legal</p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Terms & Condition</Link>
                  </p>
                  <p className="sitemap-link">
                    <Link to="/privacy">Privacy Policy</Link>
                  </p>
                </div>

                <div className="col-md-4">
                  <p className="email-title mt-3">Excited to get updates from us?</p>

                  <p className="email-text py-md-3">
                    Be the first to find out early about all upcoming updates and new product
                    releases with our newsletter.
                  </p>

                  <div className="input-button my-md-2">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your email address"
                    />
                    <button type="submit" className="btn btn-primary btn-sm mb-0">
                      Subscribe
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </footer>
      </main>
    </>
  );
};

export default Homepage;
