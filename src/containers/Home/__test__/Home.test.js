import React from "react";
import Home from "../Home";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../../redux/store/store";
import { BrowserRouter } from "react-router-dom";

function textContentMatcher(text) {
  return function (_content, node) {
    const hasText = (node) => node.textContent === text;
    const nodeHasText = hasText(node);
    const childrenDontHaveText = Array.from(node?.children || []).every(
      (child) => !hasText(child)
    );
    return nodeHasText && childrenDontHaveText;
  };
}

describe("Home", () => {
  it("should render the Home page", () => {
    const { findByText } = render(
      <Provider store={store}>
        <BrowserRouter>
          <Home />
        </BrowserRouter>
      </Provider>
    );
    expect(findByText(textContentMatcher("Mirapay"))).not.toBeNull();
  });
});

const name = "hello";
