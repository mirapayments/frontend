// Import the necessary libraries and modules.
import React, { useState } from "react";
import "./TransactionStatusPage.css";
import Success from "./gif/successful.gif";
import Failed from "./gif/failed.gif";


const TransactionStatusPage = () => {
    const [transactionStatus] = useState("successful");
  return transactionStatus === "successful"? (
    <>
   <div className="main-container">
    <div className="content-container">
        <p>Your transaction was {transactionStatus}</p>
        <br/>
        <img className="success" src={Success} alt="success" />
    </div>
   </div>
    </>
  ):(
    <>
   <div className="main-container">
    <div className="content-container">
        <p>Your transaction {transactionStatus}</p>
        <br/>
        <img className="failed" src={Failed} alt="failded" />
    </div>
   </div>
    </>
  );
  
  ;
};

// Export the component
export default TransactionStatusPage;
