function getFromLocal(key) {
  return localStorage.getItem(key);
}
function saveToLocal(key, value) {
  localStorage.setItem(key, value);
}

function removeFromLocal(key) {
  localStorage.removeItem(key);
}

export { getFromLocal, saveToLocal, removeFromLocal };
