import axios from "axios";

// base url depending on whether live or dev
const baseUrl =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

// remove  console logs from prod
if (process.env.NODE_ENV === "production") {
  console.log = function () {};
}

//create an axios instance for subsequent calls
const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_TEST_WITH_LIVE_SERVER == "yes" ? "https://api.mirapayments.com" : baseUrl,
  headers: {
    "Content-Type": "application/json",
  },
});
axiosClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    // return error.response
    let res = error.response;
    // redirect rules for all 401 responses to another page
    if (res.status >= 401 || res.status < 500) {
      return res;
    }
    return error;
    // return Promise.reject(error)
  }
);

export { axiosClient };
