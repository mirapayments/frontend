import { axiosClient } from "../axios/apiClientWithToken";

export function verifyToken(data) {
  return axiosClient.post(`/users/verify/`, data);
}
