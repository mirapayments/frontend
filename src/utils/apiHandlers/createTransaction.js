import { axiosClient } from "../axios/apiClient";

export function createTransaction(account_number, data) {
  return axiosClient.post(`/transactions/create/${account_number}/`, data);
}