import { axiosClient } from "../axios/apiClientWithToken";

export function changeAccountCredentials(account_number, data) {
  return axiosClient.post(`/accounts/credentials/${account_number}/`, data);
}
