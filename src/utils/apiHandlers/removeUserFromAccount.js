import { axiosClient } from "../axios/apiClientWithToken";

export function removeUserFromAccount(account_number, user_id) {
  return axiosClient.post(
    `/accounts/remove-user/${account_number}/${user_id}/`
  );
}
