import { axiosClient } from "../axios/apiClientWithToken";

export async function getAccountTypes() {
  return axiosClient.get("/accounts/account-types/");
}
