import { axiosClient } from "../axios/apiClient";

export async function getCurrencyTypes() {
  return axiosClient.get("/accounts/currencies/");
}
