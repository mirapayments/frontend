import { axiosClient } from "../axios/apiClientWithToken";

export function listUserAccounts() {
  return axiosClient.get("/users/me/accounts/");
}
