// Import axios client.
import { axiosClient } from "../axios/apiClient";

// Export contact-us request.
export function contactUs(data) {
    return axiosClient.post('/users/contact-us/', JSON.stringify(data)); 
    }