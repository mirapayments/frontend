import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  tabPosition: "1",
  modalTabPosition: "1",
  paymentLinkUpdated: false,
  paymentLink:{},
  currencies: [
    {
      value: "USD",
      label: "USD",
    },
    {
      value: "EUR",
      label: "EUR",
    },
    {
      value: "NGN",
      label: "NGN",
    },
    {
      value: "BGP",
      label: "BGP",
    },
  ],
};

export const uiSlice = createSlice({
  name: "ui",
  initialState,
  reducers: {
    setTabPosition: (state, action) => {
      state.tabPosition = action.payload;
    },
    setPayLink: (state, action)=>{
      state.paymentLink = action.payload;
    },
    setPaymentLinkUpdated: (state, action) => {
      state.paymentLinkUpdated = action.payload;
    },
    setCurrency: (state, action) => {
      state.currency = action.payload;
    },
    setModalTabPosition:(state, action) => {
      state.currency = action.payload;
    },
    setPaymentLinkAmount:(state, action) => {
      state.paymentLink = {...state.paymentLink, amount: action.payload }
    }
  },
});

// Action creators are generated for each case reducer function
export const { setTabPosition, setPaymentLinkUpdated, setCurrency,setPayLink, setModalTabPosition, setPaymentLinkAmount } =
  uiSlice.actions;

export default uiSlice.reducer;
