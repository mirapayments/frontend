import { rest } from "msw";
//import meResponse from "../fixures/me.json";
import currenciesResponse from "../fixtures/currencies.json";
import paymentsResponse from "../fixtures/payments.json";

const base =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:8000"
    : "https://api.mirapayments.com";

const baseUrl =
  process.env.REACT_APP_TEST_WITH_LIVE_SERVER == "yes"
    ? "https://api.mirapayments.com"
    : base;

export const handlers = [
  rest.get(`${baseUrl}/accounts/currencies/`, (req, res, ctx) => {
    return res(ctx.json(currenciesResponse));
  }),

  rest.get(
    `${baseUrl}/payments/payment-link/code/undefined/`,
    (req, res, ctx) => {
      return res(ctx.json(paymentsResponse));
    }
  ),
];
