const path = require("path");
const express = require("express");
const app = express();
const publicPath = path.join(__dirname, "..", "inline");
const port = process.env.PORT || 1337;
app.use(express.static(publicPath));
app.get("*", (req, res) => {
  res.sendFile(path.join(publicPath, "index.js"));
});
app.listen(port, () => {
  console.log("Hello World I run on PORT " + port);
});
