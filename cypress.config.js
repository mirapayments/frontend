const { defineConfig } = require('cypress')

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    "watchForFileChanges": false,
    "baseUrl": "http://localhost:3000",
    "video": false,
    "defaultCommandTimeout": 5000,
    "viewportWidth": 1440,
    "viewportHeight": 900,
    "pageLoadTimeout": 5000,
    "requestTimeout": 5000
  },
  
});
