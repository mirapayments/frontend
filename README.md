# Mirapayments Frontend
# Local Setup
1.  Clone the repository
2.  `cd` into the repository
3.  Install [n](https://www.npmjs.com/package/n) to help manage your node versions, as the node version supported for this app is `14.17.4`. Use `npm install -g n` to install `n`
4.  Use `n auto` to confirm you have the correct node version installed, or to install the desired node version
5.  Run `yarm install` to install all packages and dependencies
6.  Make sure the API server has been setup
7. Run `yarn start` to start the local server
The application should now be running on port 3000 (localhost:3000)


# Development:
1. Ensure only needed packages are added and remove a package no longer in use
    from the application dependencies.

2. Ensure no sensitive data is exposed to the user or browser. 
Note: Some values are stored in the local storage.
However, never store sensitive data to the local storage

3. Provide comments that adequately describes your operation/aim/usage in the code base

4. Write tests for all your implementations

5. Before you update a line of code, make sure you fully understand what it does, else seek help from other developers

6. Always make a pull request. Do **NOT** push to the `main` branch directly.


## Available Scripts

In the project directory, you can run:

-  `yarn start` <br />
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br />
The page will reload if you make edits.<br />
You will also see any lint errors in the console.

-  `yarn test` <br />
Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

-  `yarn build` <br />
Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.  <br />
The build is minified and the filenames include the hashes.<br />Your app is ready to be deployed!
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

- `yarn eject` <br />
**Note: this is a one-way operation. Once you `eject`, you can’t go back!** <br />
If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project. <br />
Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own. 
<br />
You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

-  Learn More <br />
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started). <br />
To learn React, check out the [React documentation](https://reactjs.org/).