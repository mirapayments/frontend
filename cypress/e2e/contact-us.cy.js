describe('Contact-us Page', () => {
  it('Assert the form should work as it should', () => {
    // Visit the page
    cy.visit('/contact');
    // cy.viewport('macbook-15')
    // Confirm you are on the right page.
    cy.contains('Contact Us');
    cy.contains('We’d love to hear more about your business, and how we can best serve you.');
    // Fill the form.
    cy.get('[data-test="full-name"]').should('be.visible').type('Abdulmalik Adebayo');
    cy.get('[data-test="phone-number"]').should('be.visible').type('09039561875');
    cy.get('[data-test="business-name"]').should('be.visible').type('MiraPayment Limited');
    cy.get('[data-test="country"]').should('be.visible').select('Nigeria');
    cy.get('[data-test="business-email"]')
      .should('be.visible')
      .type('abdulmalikadebayo07@gmail.com');
    cy.get('[data-test="payment-amount"]').should('be.visible').type('MiraPayment Limited');
    cy.get('[data-test="payment-amount"]').should('be.visible').type('100000');
    cy.get('[data-test="message-input"]')
      .should('be.visible')
      .type('This is for testing purposes.');
    cy.get('[data-test="send-button"]')
      .click()
      .then(() => {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000);
        cy.contains('successfully');
      });
  });

  it('Assert the validation working perfectly', () => {
    if (
        cy
          .get('.form')
          .find('input')
          .each(($input) => {
            cy.wrap($input).should('have.value', '');
          })
      ) {
        cy.get('[data-test="send-button"]')
          .click()
          .then(() => {
            cy.get('.form')
              .find('span')
              .should('have.class', 'error-text form__form-group-error')
              .should('be.visible');
          });
      }
  });
});
