describe("Payment Link", () => {
  it("Active Payment Link should work as expected", () => {
    const activePaymentURL = "/pay/57yv35otifo6";
    // Assert that the url works fine
    cy.visit(activePaymentURL);
    cy.contains("School fees");
    cy.get('[data-test="first-name-input"]')
      .should("be.visible")
      .type("Abdulmalik");
    cy.get('[data-test="last-name-input"]')
      .should("be.visible")
      .type("Adebayo");
    cy.get('[data-test="email-input"]')
      .should("be.visible")
      .type("abdulmalikadebayo07@gmail.com");
    cy.get(".MuiSelect-nativeInput").then(($input) => {
      const value = $input.val();
      expect($input).to.have.value(value);
    });
    cy.get('[data-test="amount-input"]').then(($input) => {
      if (!$input.is(":disabled")) {
        cy.get('[data-test="amount-input"]').type(50000);
      } else {
        const value = $input.val();
        expect($input).to.have.value(value);
      }
    });

    if (
      cy
        .get('[data-test="form-container"]')
        .find("input")
        .each(($input) => {
          cy.wrap($input).should("not.have.value", "");
        })
    ) {
      cy.get('[data-test="pay-button"]').click();
      cy.wait(2000);
      cy.log("I reached here");
      cy.url().should("include", "/pay?email=");
      // test
      cy.intercept("POST", "/transactions/create/*", {
        statusCode: 201,
        fixture: "create_transaction.json",
      });

      cy.get('[data-test="card"]')
        .should("be.visible")
        .type("4957030420210454");
      cy.get('[data-test="expiryDate"]').type("1040", { force: true });
      cy.get('[data-test="cvv"]').type("999", { force: true });
      cy.get('[data-test="cardHolder"]').should("be.visible").type("Test Card");
      cy.get('[data-test="cardSubmitButton"]')
        .click()
        .then(() => {
          cy.log("I reached here");
          cy.wait(2000);
          cy.url().should("include", "/transaction/status");
          cy.contains("Your transaction was successful");
        });
    } else {
      cy.get('[data-test="pay-button"]')
        .click()
        .then(() => {
          cy.get('[data-test="form-container"]')
            .find("p")
            .should("have.class", "error_validation_text")
            .should("be.visible");
        });
    }
  });

  it("Inactive payment link should work as expected", () => {
    const inactivePaymentURL = "/pay/qqeffbpwagd2";
    cy.visit(inactivePaymentURL);
    cy.contains(
      "Payment Link expired, or deleted. Please contact owner to create a new link and share with you."
    );
  });
});
