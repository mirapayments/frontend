describe('Assert all pages are working fine', () => {
  it('passes', () => {
    // Visit the page
    cy.visit('/');
    // Should meet the header.
    cy.contains('Easy online payment gateway for everyone');
  });
});
